import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Req,
  Res,
  Get,
  Put,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { AdminsService } from './admins.service';
import { AdminDto, LoginDto, UpdatePasswordDto } from '../../DTO/admin.dto';

@ApiTags('admins')
@Controller('admins')
export class AdminController {
  constructor(private readonly adminsService: AdminsService) {}
  @ApiOkResponse({
    description: 'admin login successful',
    type: [AdminDto],
  })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'login faild',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  async login(@Req() req: Request, @Body() login: LoginDto): Promise<AdminDto> {
    try {
      if (req.session && req.session.admin) {
        return req.session.admin;
      }
      const admin = await this.adminsService.login(
        login.userName,
        login.password,
      );
      req.session.admin = admin;
      return admin;
    } catch (err) {
      throw err;
    }
  }
  @Get('')
  @HttpCode(HttpStatus.OK)
  async dash(@Res() res: Response): Promise<AdminDto> {
    res.json(res.locals.admin);
    return;
  }
  @Put('updatePassword')
  @HttpCode(HttpStatus.OK)
  async updatePassword(
    @Req() req: Request,
    @Res() res: Response,
    @Body() updatePasswordDto: UpdatePasswordDto,
  ) {
    try {
      const { admin } = res.locals;
      await this.adminsService.updatePassword(
        updatePasswordDto.oldPassword,
        updatePasswordDto.newPassword,
        admin.userName,
      );
      res.json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
}
