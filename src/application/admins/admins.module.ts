import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { AdminController } from './admins.controller';
import { AdminsService } from './admins.service';
import { BasicDefinitionModule } from './basicDefinitions/basicDefinitions.module';
import { LawyerModule } from './lawyers/lawyer.module';
import { BlogModule } from './blog/blog.module';
import { AdminsDataAcceess } from '../../dataAccess/admins.dataAccess';
import {
  ValidAdminMiddleware,
  CheckAdminMiddleware,
} from '../../common/middlewares/validateAdmin.middleware';
import { Jwt } from '../../common/helpers/jwt.helper';
import { ConvertDate } from '../../common/helpers/convertDate.helper';

@Module({
  imports: [BasicDefinitionModule, LawyerModule, BlogModule],
  controllers: [AdminController],
  providers: [AdminsService, AdminsDataAcceess, Jwt, ConvertDate],
})
export class AdminModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes(
      'admin/blog',
      'admin/lawyers',
      {
        path: 'admins',
        method: RequestMethod.GET,
      },
      {
        path: 'admins/updatePassword',
        method: RequestMethod.PUT,
      },
    );
    consumer.apply(CheckAdminMiddleware).forRoutes({
      path: 'admins',
      method: RequestMethod.POST,
    });
  }
}
