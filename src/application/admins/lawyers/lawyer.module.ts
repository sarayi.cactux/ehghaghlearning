import { Module } from '@nestjs/common';

import { LawyerController } from './lawyer.controller';
import { LawyerService } from './lawyer.service';
import { DepartmentDataAcceess } from '../../../dataAccess/department.dataAccess';
import { AdminsDataAcceess } from '../../../dataAccess/admins.dataAccess';
import { LawyersDataAcceess } from '../../../dataAccess/lawyers.dataAccess';
import { UsersDataAcceess } from '../../../dataAccess/users.dataAccess';
import { ConvertDate } from '../../../common/helpers/convertDate.helper';
import { Tools } from '../../../common/helpers/tools.helper';
import { Jwt } from '../../../common/helpers/jwt.helper';

@Module({
  controllers: [LawyerController],
  providers: [
    LawyerService,
    DepartmentDataAcceess,
    AdminsDataAcceess,
    LawyersDataAcceess,
    UsersDataAcceess,
    ConvertDate,
    Tools,
    Jwt,
  ],
})
export class LawyerModule {}
