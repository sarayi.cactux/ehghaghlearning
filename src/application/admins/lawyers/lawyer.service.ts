import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { LawyersDataAcceess } from '../../../dataAccess/lawyers.dataAccess';
import { Tools } from '../../../common/helpers/tools.helper';
import { LawyerListDto, lawyerListObj } from '../../../DTO/lawyerInf.dto';
import lawyerStatuses from '../../../common/eNums/lawyerStatuses.enum';

@Injectable()
export class LawyerService {
  constructor(
    private readonly lawyersDataAcceess: LawyersDataAcceess,
    private readonly tools: Tools,
  ) {}
  async listLawyers(filterlawyerDto): Promise<LawyerListDto[]> {
    const list = await this.lawyersDataAcceess.listAndFilter(filterlawyerDto);
    return list.map((item) => lawyerListObj(item));
  }
  async updateAcceptStatus(updateAcceptStatusLawyer) {
    const { id, acceptStatus, rejectDescription } = updateAcceptStatusLawyer;
    const lawyer = await this.lawyersDataAcceess.findById(id);
    if (!lawyer) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'LAWYER_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkAcceptStatus = Object.keys(lawyerStatuses).some(
      (key) => lawyerStatuses[key].code === acceptStatus,
    );
    if (!checkAcceptStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'INVALIED_acceptStatus',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.lawyersDataAcceess.updateAcceptStatus(
      id,
      acceptStatus,
      rejectDescription,
    );
  }
}
