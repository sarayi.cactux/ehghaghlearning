import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Param,
  ParseIntPipe,
  Query,
  Put,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiHeader,
  ApiBody,
  ApiOperation,
} from '@nestjs/swagger';
import { Response } from 'express';
import { LawyerService } from './lawyer.service';
import {
  FilterlawyerDto,
  LawyerListDto,
  UpdateAcceptStatusLawyerDto,
} from '../../..//DTO/lawyerInf.dto';

@ApiTags('administrator  lawyers')
@Controller('admin/lawyers')
export class LawyerController {
  constructor(private readonly lawyerService: LawyerService) {}
  // list lawyers ***********************************************************************
  @ApiOperation({ summary: 'get lawyers list and filter list' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'lawyers list',
    type: [LawyerListDto],
  })
  @Get('')
  @HttpCode(HttpStatus.OK)
  async listLawyers(
    @Query() filterlawyerDto: FilterlawyerDto,
  ): Promise<LawyerListDto[]> {
    const lawyerService = await this.lawyerService.listLawyers(filterlawyerDto);
    return lawyerService;
  }
  // update AcceptStatus ***********************************************************************
  @ApiOperation({ summary: 'update aacceptStatus' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @Put('')
  @HttpCode(HttpStatus.OK)
  async updateAcceptStatus(
    @Body() updateAcceptStatusLawyer: UpdateAcceptStatusLawyerDto,
  ): Promise<any> {
    try {
      await this.lawyerService.updateAcceptStatus(updateAcceptStatusLawyer);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
}
