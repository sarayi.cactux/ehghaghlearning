import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import md5 = require('md5');
import { AdminDto, adminObj } from '../../DTO/admin.dto';
import { Jwt } from '../../common/helpers/jwt.helper';
import { AdminsDataAcceess } from '../../dataAccess/admins.dataAccess';

@Injectable()
export class AdminsService {
  constructor(
    private readonly adminsDataAcceess: AdminsDataAcceess,
    private readonly jwt: Jwt,
  ) {}
  async login(userName: string, password: string): Promise<AdminDto> {
    const admin = await this.adminsDataAcceess.findByUserName(userName);
    if (!admin) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'LOGIN_FAILD',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (admin.password !== md5(password)) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'LOGIN_FAILD',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const tokenValues = {
      adminId: admin.id,
    };
    const token = this.jwt.signer(tokenValues, 86400);
    await this.adminsDataAcceess.updateJwtToken(token, admin.id);
    return adminObj(admin, token);
  }
  async updatePassword(oldPassword, newPassword, userName) {
    const admin = await this.adminsDataAcceess.findByUserName(userName);
    if (admin.password === md5(oldPassword)) {
      await this.adminsDataAcceess.updatePassword(admin.id, md5(newPassword));
      return;
    }
    throw new HttpException(
      {
        status: HttpStatus.FORBIDDEN,
        error: 'UPDATE_PASSWORD_FORBIDEN',
      },
      HttpStatus.FORBIDDEN,
    );
  }
}
