import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import Promise = require('bluebird');

import { CategoryDataAccess } from '../../../dataAccess/blogCategory.dataAccess';
import { PostDataAccess } from '../../../dataAccess/post.dataAccess';
import { MediaDataAcceess } from '../../../dataAccess/media.dataAccess';
import { PostMediaDataAcceess } from '../../../dataAccess/postMedia.dataAccess';
import { NewsMediaDataAcceess } from '../../../dataAccess/newsMedia.dataAccess';
import { NewsCategoryDataAccess } from '../../../dataAccess/newsCategory.dataAccess';
import { NewsDataAccess } from '../../../dataAccess/news.dataAccess';
import { Tools } from '../../../common/helpers/tools.helper';
import {
  BlogCatDto,
  BlogCatListDto,
  blogCatObjDto,
} from '../../../DTO/blogCategory.dto';
import {
  newsCatObjDto,
  NewsCatDto,
  NewsCatListDto,
} from '../../../DTO/newsCategory.dto';
import {
  PostDto,
  postObjDto,
  ListPostDto,
  postInfoObjDto,
} from '../../../DTO/post.dto';
import {
  NewsDto,
  newsObjDto,
  ListNewsDto,
  newsInfoObjDto,
} from '../../../DTO/news.dto';
import blogStatuses from '../../../common/eNums/blogStatuses.enum ';

@Injectable()
export class BlogService {
  constructor(
    private readonly mediaDataAcceess: MediaDataAcceess,
    private readonly categoryDataAccess: CategoryDataAccess,
    private readonly postDataAccess: PostDataAccess,
    private readonly newsDataAccess: NewsDataAccess,
    private readonly newsCategoryDataAccess: NewsCategoryDataAccess,
    private readonly postMediaDataAcceess: PostMediaDataAcceess,
    private readonly newsMediaDataAccess: NewsMediaDataAcceess,
    private readonly tools: Tools,
  ) {}
  // create post ******************************************************************
  async createPost(createPostDto, admin): Promise<PostDto> {
    // eslint-disable-next-line object-curly-newline
    const { blogCatId, mediaId, title, text, isInLanding, postMedias } =
      createPostDto;
    const blogCat = await this.categoryDataAccess.findById(blogCatId);
    if (!blogCat) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_invalid_blogCatId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const slug = this.tools.convertToSlug(title);
    const checkPost = await this.postDataAccess.checkDuplicate(slug);
    if (checkPost) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'CONFLICT_duplicate_post_title',
        },
        HttpStatus.CONFLICT,
      );
    }
    const media = await this.mediaDataAcceess.findById(mediaId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_media_by_mediaId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const post = await this.postDataAccess.createPostRequest(
      admin.id,
      blogCatId,
      mediaId,
      title,
      slug,
      text,
      isInLanding,
    );
    await this.postDataAccess.updateSortPost(post.id);
    if (
      typeof postMedias === 'object' &&
      postMedias.constructor === Array &&
      postMedias.length > 0
    ) {
      await Promise.map(postMedias, async (media) => {
        const item = await this.mediaDataAcceess.findById(media.mediaId);
        if (item) {
          const postMedia = await this.postMediaDataAcceess.create(
            media.mediaId,
            post.id,
            media.title,
          );
          this.mediaDataAcceess.updateMediaOwner(
            postMedia.id,
            'PostMedia',
            media.mediaId,
          );
        }
      });
    }
    this.mediaDataAcceess.updateMediaOwner(post.id, 'Post', mediaId);
    return postObjDto(await this.postDataAccess.findById(post.id));
  }
  // update post *********************************************************************
  async updatePost(updatePostDto, admin) {
    // eslint-disable-next-line object-curly-newline
    const { id, blogCatId, mediaId, title, text, isInLanding, postMedias } =
      updatePostDto;
    const blogCat = await this.categoryDataAccess.findById(blogCatId);
    if (!blogCat) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_invalid_blogCatId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const post = await this.postDataAccess.findById(id);
    if (!post) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const media = await this.mediaDataAcceess.findById(mediaId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_media_by_mediaId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.postDataAccess.updatePost(
      id,
      admin.id,
      blogCatId,
      mediaId,
      title,
      text,
      isInLanding,
    );
    if (
      typeof postMedias === 'object' &&
      postMedias.constructor === Array &&
      postMedias.length > 0
    ) {
      await Promise.map(postMedias, async (media) => {
        const item = await this.mediaDataAcceess.findById(media.mediaId);
        const postMedia = await this.postMediaDataAcceess.find(
          post.id,
          media.mediaId,
        );
        if (item && !postMedia) {
          const postMedia = await this.postMediaDataAcceess.create(
            media.mediaId,
            post.id,
            media.title,
          );
          this.mediaDataAcceess.updateMediaOwner(
            postMedia.id,
            'PostMedia',
            media.mediaId,
          );
        }
      });
    }
    await this.mediaDataAcceess.updateMediaOwner(post.id, 'Post', mediaId);
    return;
  }
  // post detail ***********************************************************************
  async postDetail(postId): Promise<PostDto> {
    const postDetail = await this.postDataAccess.findById(postId);
    if (postDetail) {
      return postInfoObjDto(postDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  // detele postMedia ***************************************************************************
  async deletePostMedia(deletePostMediaDto) {
    const { id, postId } = deletePostMediaDto;
    const media = await this.postMediaDataAcceess.findPostMedia(id, postId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POSTMEDIA_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.mediaDataAcceess.updateMediaOwner(0, '', media.mediaId);
    await this.postMediaDataAcceess.deletePostMedia(media.id);
  }
  // change status post ********************************************************************
  async changeStatusPost(postStatusDto) {
    const { id, active } = postStatusDto;
    const post = await this.postDataAccess.findById(id);
    if (!post) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'POST_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(blogStatuses).some(
      (key) => blogStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'INVALIED_ChangeStatus',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.postDataAccess.changeStatusPost(id, active);
  }
  // list posts ***************************************************************************
  async listPosts(filterListPostDto): Promise<ListPostDto[]> {
    const list = await this.postDataAccess.listAndFilter(filterListPostDto);
    return list.map((item) => postObjDto(item));
  }
  //change Sort post *************************************************************************
  async changeSortPost(
    filterListPostDto,
    changesortpostDto,
  ): Promise<ListPostDto[]> {
    const { fromId, toId } = changesortpostDto;
    await this.postDataAccess.changeSortPost(fromId, toId);
    const list = await this.postDataAccess.listAndFilter(filterListPostDto);
    return list.map((item) => postObjDto(item));
  }
  // create category *************************************************************************
  async createCategory(createBlogCatDto): Promise<BlogCatDto> {
    const { title, description } = createBlogCatDto;
    const checkTitle = await this.categoryDataAccess.checkDuplicate(title);

    if (checkTitle) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'CONFLICT_duplicate_category_title',
        },
        HttpStatus.CONFLICT,
      );
    }
    const category = await this.categoryDataAccess.createCategory(
      title,
      description,
    );
    return blogCatObjDto(category);
  }
  // list category ****************************************************************************
  async blogCatList(): Promise<BlogCatListDto[]> {
    const categories = await this.categoryDataAccess.findAllBlogCategries();
    const categoryList = categories.map((category) => blogCatObjDto(category));
    return categoryList;
  }
  // update category *************************************************************************
  async updateBlogCategory(updateBlogCategory) {
    const { id, title, description } = updateBlogCategory;
    const category = await this.categoryDataAccess.findById(id);
    if (!category) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'CATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkTitle = await this.categoryDataAccess.checkDuplicate(title);

    if (checkTitle && title !== category.title) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'CONFLICT_duplicate_category_title',
        },
        HttpStatus.CONFLICT,
      );
    }
    await this.categoryDataAccess.updateBlogCategory(id, title, description);
  }
  // change status category *************************************************************************
  async changeStatusCategory(satusBlogCatDto) {
    const { id, active } = satusBlogCatDto;
    const category = await this.categoryDataAccess.findById(id);
    if (!category) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'CATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(blogStatuses).some(
      (key) => blogStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'INVALIED_ChangeStatus',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.categoryDataAccess.changeStatusCategory(id, active);
  }
  // createNewsCategory *******************************************************************************
  async createNewsCategory(createNewsCatDto): Promise<NewsCatDto> {
    const { title, description } = createNewsCatDto;
    const checkTitle = await this.newsCategoryDataAccess.checkDuplicate(title);

    if (checkTitle) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'CONFLICT_duplicate_newscategory_title',
        },
        HttpStatus.CONFLICT,
      );
    }
    const newsCategory = await this.newsCategoryDataAccess.crateNewsCategory(
      title,
      description,
    );
    return newsCatObjDto(newsCategory);
  }
  // list NewsCategories *******************************************************************************
  async newsCategoryList(): Promise<NewsCatListDto[]> {
    const newsCategories = await this.newsCategoryDataAccess.findAll();
    const newsCategoyList = newsCategories.map((category) =>
      newsCatObjDto(category),
    );
    return newsCategoyList;
  }
  // update NewsCategories *******************************************************************************
  async updateNewsCategory(updateNewsCategory) {
    const { id, title, description } = updateNewsCategory;
    const newsCategory = await this.newsCategoryDataAccess.findById(id);
    if (!newsCategory) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NEWSCATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkTitle = await this.newsCategoryDataAccess.checkDuplicate(title);

    if (checkTitle && title !== newsCategory.title) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'CONFLICT_duplicate_newscategory_title',
        },
        HttpStatus.CONFLICT,
      );
    }
    await this.newsCategoryDataAccess.updateNewsCategory(
      id,
      title,
      description,
    );
  }
  // change status NewsCategory *************************************************************************
  async changeStatusNewsCategory(statusNewsCatDto) {
    const { id, active } = statusNewsCatDto;
    const newsCategory = await this.newsCategoryDataAccess.findById(id);
    if (!newsCategory) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NEWSCATEGORY_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(blogStatuses).some(
      (key) => blogStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'INVALIED_ChangeStatus',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.newsCategoryDataAccess.changeStatusNewsCategory(id, active);
  }
  // create new news *****************************************************************************************
  async createNewsRequest(createNewsDto, admin): Promise<NewsDto> {
    const {
      newsCatId,
      mediaId,
      title,
      text,
      source,
      sourceLink,
      isInLanding,
      newsMedias,
    } = createNewsDto;
    const NewsCategory = await this.newsCategoryDataAccess.findById(newsCatId);
    console.log(admin.id);
    if (!NewsCategory) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_invalid_NewsCatId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const slug = this.tools.convertToSlug(title);
    const checkNews = await this.newsDataAccess.checkDuplicate(slug);
    if (checkNews) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'CONFLICT_duplicate_News_title',
        },
        HttpStatus.CONFLICT,
      );
    }
    const media = await this.mediaDataAcceess.findById(mediaId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_media_by_mediaId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const news = await this.newsDataAccess.createNewsRequest(
      admin.id,
      newsCatId,
      mediaId,
      title,
      slug,
      text,
      source,
      sourceLink,
      isInLanding,
    );
    await this.newsDataAccess.updateSortNews(news.id);
    if (
      typeof newsMedias === 'object' &&
      newsMedias.constructor === Array &&
      newsMedias.length > 0
    ) {
      await Promise.map(newsMedias, async (media) => {
        const item = await this.mediaDataAcceess.findById(media.mediaId);
        if (item) {
          const newsMedia = await this.newsMediaDataAccess.create(
            media.mediaId,
            news.id,
            media.title,
          );
          this.mediaDataAcceess.updateMediaOwner(
            newsMedia.id,
            'NewsMedia',
            media.mediaId,
          );
        }
      });
    }
    this.mediaDataAcceess.updateMediaOwner(news.id, 'News', mediaId);
    return newsObjDto(await this.newsDataAccess.findById(news.id));
  }
  // update news ****************************************************************
  async updateNews(updateNewsDto, admin) {
    const {
      id,
      newsCatId,
      mediaId,
      title,
      text,
      source,
      sourceLink,
      isInLanding,
      newsMedias,
    } = updateNewsDto;
    const newsCat = await this.newsCategoryDataAccess.findById(newsCatId);
    if (!newsCat) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_invalid_newsCatId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const news = await this.newsDataAccess.findById(id);
    if (!news) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'News_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const media = await this.mediaDataAcceess.findById(mediaId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND_media_by_mediaId',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.newsDataAccess.updateNews(
      id,
      admin.id,
      newsCatId,
      mediaId,
      title,
      text,
      source,
      sourceLink,
      isInLanding,
    );
    if (
      typeof newsMedias === 'object' &&
      newsMedias.constructor === Array &&
      newsMedias.length > 0
    ) {
      await Promise.map(newsMedias, async (media) => {
        const item = await this.mediaDataAcceess.findById(media.mediaId);
        const newsMedia = await this.newsMediaDataAccess.find(
          news.id,
          media.mediaId,
        );
        if (item && !newsMedia) {
          const newsMedia = await this.newsMediaDataAccess.create(
            news.mediaId,
            news.id,
            media.title,
          );
          this.mediaDataAcceess.updateMediaOwner(
            newsMedia.id,
            'NewsMedia',
            media.mediaId,
          );
        }
      });
    }
    await this.mediaDataAcceess.updateMediaOwner(news.id, 'News', mediaId);
    return;
  }
  // change status of news ******************************************************************
  async changeStatusNews(newsStatusDto) {
    const { id, active } = newsStatusDto;
    const news = await this.newsDataAccess.findById(id);
    if (!news) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NEWS_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const checkStatus = Object.keys(blogStatuses).some(
      (key) => blogStatuses[key].code === active,
    );
    if (!checkStatus) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'INVALIED_ChangeStatus',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.newsDataAccess.changeStatusNews(id, active);
  }
  // list newses *****************************************************************************
  async listNewses(filterListNewsDto): Promise<ListNewsDto[]> {
    const list = await this.newsDataAccess.listAndFilter(filterListNewsDto);
    return list.map((item) => newsObjDto(item));
  }
  // news detail *****************************************************************************
  async newsDetail(newsId): Promise<NewsDto> {
    const newsDetail = await this.newsDataAccess.findById(newsId);
    if (newsDetail) {
      return newsInfoObjDto(newsDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  // delete News media ************************************************************************
  async deleteNewsMedia(deleteNewsMediaDto) {
    const { id, newsId } = deleteNewsMediaDto;
    const media = await this.newsMediaDataAccess.findNewsMedia(id, newsId);
    if (!media) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NEWSMEDIA_NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.mediaDataAcceess.updateMediaOwner(0, '', media.mediaId);
    await this.newsMediaDataAccess.deleteNewsMedia(media.id);
  }
  // change sort news *****************************************************************************
  async changeSortNews(
    changeSortNewsDto,
    filterListNewsDto,
  ): Promise<ListNewsDto[]> {
    const { fromId, toId } = changeSortNewsDto;
    await this.newsDataAccess.changeSortNews(fromId, toId);
    const list = await this.newsDataAccess.listAndFilter(filterListNewsDto);
    return list.map((item) => newsObjDto(item));
  }
}
