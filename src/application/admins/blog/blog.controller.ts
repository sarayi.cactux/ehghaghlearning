import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Patch,
  Param,
  ParseIntPipe,
  Delete,
  HttpException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiHeader,
  ApiBody,
  ApiOperation,
} from '@nestjs/swagger';
import { Response } from 'express';
import {
  BlogCatDto,
  CreateBlogCatDto,
  UpdateBlogCatDto,
  StatusBlogCatDto,
  BlogCatListDto,
} from '../../..//DTO/blogCategory.dto';
import {
  NewsCatDto,
  CreateNewsCatDto,
  UpdateNewsCatDto,
  StatusNewsCatDto,
  NewsCatListDto,
} from '../../..//DTO/newsCategory.dto';
import {
  PostDto,
  CreatePostDto,
  UpdatePostDto,
  PostStatusDto,
  ListPostDto,
  FilterListPostDto,
  ChangeSortPostDto,
} from '../../../DTO/post.dto';
import {
  NewsDto,
  CreateNewsDto,
  UpdateNewsDto,
  NewsStatusDto,
  ListNewsDto,
  FilterListNewsDto,
  ChangeSortNewsDto,
} from '../../../DTO/news.dto';
import { DeleteNewsMediaDto } from '../../../DTO/newsMedia.dto';
import { DeletePostMediaDto } from '../../../DTO/postMedia.dto';
import { BlogService } from './blog.service';

@ApiTags('blog')
@Controller('admin/blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}
  // create new post **************************************************************************
  @ApiOperation({ summary: 'create new post' })
  @ApiOkResponse({
    description: 'PostDto info',
    type: [PostDto],
  })
  @ApiBody({
    type: CreatePostDto,
    description: 'create new post',
  })
  @Post('post')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createPost(
    @Res() res: Response,
    @Body() createPostDto: CreatePostDto,
  ): Promise<PostDto> {
    try {
      const post = await this.blogService.createPost(
        createPostDto,
        res.locals.admin,
      );
      res.status(HttpStatus.OK).json(post);
      return;
    } catch (err) {
      throw err;
    }
  }
  // update post ******************************************************************************
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'update post' })
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'update post',
    type: Boolean,
  })
  @ApiBody({
    type: UpdatePostDto,
    description: 'update post DTO',
  })
  @Put('post')
  @HttpCode(HttpStatus.OK)
  async updatePost(
    @Res() res: Response,
    @Body() updatePostDto: UpdatePostDto,
  ): Promise<any> {
    try {
      await this.blogService.updatePost(updatePostDto, res.locals.admin);
      res.json({ status: true });
      return;
    } catch (err) {
      throw err;
    }
  }
  // change status of post ********************************************************************
  @ApiOperation({ summary: 'change status of post' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: PostStatusDto,
    description: 'change Status of post',
  })
  @Patch('post')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async changeStatusPost(@Body() postStatusDto: PostStatusDto): Promise<any> {
    try {
      await this.blogService.changeStatusPost(postStatusDto);
      return { status: true };
      return;
    } catch (err) {
      throw err;
    }
  }
  // Filter / list post ***********************************************************************
  @ApiOperation({ summary: ' Filter / list post ' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'posts list',
    type: [ListPostDto],
  })
  @Get('post')
  @HttpCode(HttpStatus.OK)
  async ListPosts(
    @Query() filterListPostDto: FilterListPostDto,
  ): Promise<ListPostDto[]> {
    const blogService = await this.blogService.listPosts(filterListPostDto);
    return blogService;
  }
  // get  detail post by id *****************************************************************************
  @ApiOperation({ summary: 'detail post' })
  @ApiOkResponse({
    description: 'post detail info',
    type: PostDto,
  })
  @Get('post/:postId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async postDetail(
    @Param(
      'postId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    postId: number,
  ): Promise<PostDto> {
    try {
      const postDetail = await this.blogService.postDetail(postId);
      return postDetail;
    } catch (err) {
      throw err;
    }
  }
  // delete post media ************************************************************************
  @ApiOperation({ summary: 'delete post media' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'detele post media',
    type: Boolean,
  })
  @ApiBody({
    type: DeletePostMediaDto,
    description: 'delete post media info',
  })
  @Delete('post/deletePostMedia')
  @HttpCode(HttpStatus.OK)
  async detelePostMedia(
    @Body() deletePostMediaDto: DeletePostMediaDto,
  ): Promise<any> {
    try {
      await this.blogService.deletePostMedia(deletePostMediaDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // change sort post ************************************************************************
  @ApiOperation({ summary: 'change sort post' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'change sort post',
    type: [ListPostDto],
  })
  @ApiBody({
    type: ChangeSortPostDto,
    description: 'change sort post',
  })
  @Put('post/sort')
  @HttpCode(HttpStatus.OK)
  async changeSortPost(
    @Body() changesortpostDto: ChangeSortPostDto,
    @Query() filterListPostDto: FilterListPostDto,
  ): Promise<ListPostDto[]> {
    try {
      const listPosts = await this.blogService.changeSortPost(
        filterListPostDto,
        changesortpostDto,
      );
      return listPosts;
    } catch (err) {
      throw err;
    }
  }
  // create new blogCat ***********************************************************************
  @ApiOperation({ summary: ' create blog category' })
  @ApiOkResponse({
    description: 'blogCatagoryDto info',
    type: [BlogCatDto],
  })
  @ApiBody({
    type: CreateBlogCatDto,
    description: 'create new blog category',
  })
  @Post('category')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createCategory(
    @Body() createBlogCatDto: CreateBlogCatDto,
  ): Promise<BlogCatDto> {
    try {
      const category = await this.blogService.createCategory(createBlogCatDto);
      return category;
    } catch (err) {
      throw err;
    }
  }
  // list blogCat ***********************************************************************
  @ApiOperation({ summary: ' list blog category' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'blog category list',
    type: [BlogCatListDto],
  })
  @Get('category')
  @HttpCode(HttpStatus.OK)
  async blogCatList(): Promise<BlogCatListDto[]> {
    try {
      const categories = await this.blogService.blogCatList();
      return categories;
    } catch (err) {
      throw err;
    }
  }
  // update blogCat **********************************************************************
  @ApiOperation({ summary: ' update blog category' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'update blog category',
    type: Boolean,
  })
  @ApiBody({
    type: UpdateBlogCatDto,
    description: 'update blog category',
  })
  @Put('category')
  @HttpCode(HttpStatus.OK)
  async updateBlogCategory(
    @Body() updateBlogCategory: UpdateBlogCatDto,
  ): Promise<any> {
    try {
      await this.blogService.updateBlogCategory(updateBlogCategory);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  //change status of blog Category **********************************************************************
  @ApiOperation({ summary: ' change status blog category' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: StatusBlogCatDto,
    description: 'change Status of blog category',
  })
  @Patch('category')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async changeStatusCategory(
    @Body() satusBlogCatDto: StatusBlogCatDto,
  ): Promise<any> {
    try {
      await this.blogService.changeStatusCategory(satusBlogCatDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // create new NewsCategory *****************************************************************************
  @ApiOperation({ summary: ' create newsCategory' })
  @ApiOkResponse({
    description: 'newsCatagoryDto info',
    type: [NewsCatDto],
  })
  @ApiBody({
    type: CreateNewsCatDto,
    description: 'create new news category',
  })
  @Post('newsCategory')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createNewsCategory(
    @Body() createNewsCatDto: CreateNewsCatDto,
  ): Promise<NewsCatDto> {
    try {
      const newsCategory = await this.blogService.createNewsCategory(
        createNewsCatDto,
      );
      return newsCategory;
    } catch (err) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'INTERNAL_SERVER_ERROR',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  // list NewsCategory *****************************************************************
  @ApiOperation({ summary: ' list newsCategory' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'newsCategory list',
    type: [NewsCatListDto],
  })
  @Get('newsCategory')
  @HttpCode(HttpStatus.OK)
  async newsCategoryList(): Promise<NewsCatListDto[]> {
    try {
      const categories = await this.blogService.newsCategoryList();
      return categories;
    } catch (err) {
      throw err;
    }
  }
  // update NewsCategory ************************************************************************
  @ApiOperation({ summary: ' update newsCategory' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'update NewsCategory',
    type: Boolean,
  })
  @ApiBody({
    type: UpdateBlogCatDto,
    description: 'update NewsCategory info',
  })
  @Put('newsCategory')
  @HttpCode(HttpStatus.OK)
  async updateNewsCategory(
    @Body() updateNewsCategory: UpdateNewsCatDto,
  ): Promise<any> {
    try {
      await this.blogService.updateNewsCategory(updateNewsCategory);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // change status of NewsCategory *********************************************************************************
  @ApiOperation({ summary: 'change status NewsCategory' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: StatusNewsCatDto,
    description: 'chanage status of NewsCategory',
  })
  @Patch('newsCategory')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async changeStatusNewsCategory(
    @Body() statusNewsCatDto: StatusBlogCatDto,
  ): Promise<any> {
    try {
      await this.blogService.changeStatusNewsCategory(statusNewsCatDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // create new News ************************************************************************************************
  @ApiOperation({ summary: 'create new News' })
  @ApiOkResponse({
    description: 'NewsDto info',
    type: [NewsDto],
  })
  @ApiBody({
    type: CreateNewsDto,
    description: 'create new News',
  })
  @Post('news')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createNewsRequest(
    @Res() res: Response,
    @Body() createNewsDto: CreateNewsDto,
  ): Promise<PostDto> {
    try {
      const news = await this.blogService.createNewsRequest(
        createNewsDto,
        res.locals.admin,
      );
      res.status(HttpStatus.OK).json(news);
      return;
    } catch (err) {
      throw err;
    }
  }
  // update News ************************************************************************************************
  @ApiOperation({ summary: 'update News' })
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'update news',
    type: Boolean,
  })
  @ApiBody({
    type: UpdateNewsDto,
    description: 'update news DTO',
  })
  @Put('news')
  @HttpCode(HttpStatus.OK)
  async updateNews(
    @Res() res: Response,
    @Body() updateNewsDto: UpdateNewsDto,
  ): Promise<any> {
    try {
      await this.blogService.updateNews(updateNewsDto, res.locals.admin);
      res.json({ status: true });
      return;
    } catch (err) {
      throw err;
    }
  }
  // change status of news ********************************************************************
  @ApiOperation({ summary: 'change status of news' })
  @ApiOkResponse({
    description: 'api status',
    type: Boolean,
  })
  @ApiBody({
    type: NewsStatusDto,
    description: 'change Status of news',
  })
  @Patch('news')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async changeStatusNews(@Body() newsStatusDto: NewsStatusDto): Promise<any> {
    try {
      await this.blogService.changeStatusNews(newsStatusDto);
      return { status: true };
      return;
    } catch (err) {
      throw err;
    }
  }
  // Filter / list News ***********************************************************************
  @ApiOperation({ summary: ' Filter / list news ' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'news list',
    type: [ListNewsDto],
  })
  @Get('news')
  @HttpCode(HttpStatus.OK)
  async ListNewses(
    @Query() filterListNewsDto: FilterListNewsDto,
  ): Promise<ListNewsDto[]> {
    const newsList = await this.blogService.listNewses(filterListNewsDto);
    return newsList;
  }
  // get  detail news by id *****************************************************************************
  @ApiOperation({ summary: 'detail news' })
  @ApiOkResponse({
    description: 'news detail info',
    type: NewsDto,
  })
  @Get('news/:newsId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async newsDetail(
    @Param(
      'newsId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    newsId: number,
  ): Promise<NewsDto> {
    try {
      const newsDetail = await this.blogService.newsDetail(newsId);
      return newsDetail;
    } catch (err) {
      throw err;
    }
  }
  // delete News media ************************************************************************
  @ApiOperation({ summary: 'delete news media' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'detele news media',
    type: Boolean,
  })
  @ApiBody({
    type: DeleteNewsMediaDto,
    description: 'delete post media info',
  })
  @Delete('news/deleteNewsMedia')
  @HttpCode(HttpStatus.OK)
  async deteleNewsMedia(
    @Body() deleteNewsMediaDto: DeleteNewsMediaDto,
  ): Promise<any> {
    try {
      await this.blogService.deleteNewsMedia(deleteNewsMediaDto);
      return { status: true };
    } catch (err) {
      throw err;
    }
  }
  // change sort news ************************************************************************
  @ApiOperation({ summary: 'change sort news' })
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'change sort news',
    type: [ListNewsDto],
  })
  @ApiBody({
    type: ChangeSortNewsDto,
    description: 'change sort news',
  })
  @Put('news/sort')
  @HttpCode(HttpStatus.OK)
  async changeSortNews(
    @Body() changeSortNewsDto: ChangeSortNewsDto,
    @Query() filterListNewsDto: FilterListNewsDto,
  ): Promise<ListNewsDto[]> {
    try {
      const listNews = await this.blogService.changeSortNews(
        changeSortNewsDto,
        filterListNewsDto,
      );
      return listNews;
    } catch (err) {
      throw err;
    }
  }
}
