import { Module } from '@nestjs/common';

import { BlogController } from './blog.controller';
import { BlogService } from './blog.service';
import { PostDataAccess } from '../../../dataAccess/post.dataAccess';
import { CategoryDataAccess } from '../../../dataAccess/blogCategory.dataAccess';
import { AdminsDataAcceess } from '../../../dataAccess/admins.dataAccess';
import { MediaDataAcceess } from '../../../dataAccess/media.dataAccess';
import { PostMediaDataAcceess } from '../../../dataAccess/postMedia.dataAccess';
import { NewsMediaDataAcceess } from '../../../dataAccess/newsMedia.dataAccess';
import { NewsCategoryDataAccess } from '../../../dataAccess/newsCategory.dataAccess';
import { NewsDataAccess } from '../../../dataAccess/news.dataAccess';
import { ConvertDate } from '../../../common/helpers/convertDate.helper';
import { Tools } from '../../../common/helpers/tools.helper';
import { Jwt } from '../../../common/helpers/jwt.helper';

@Module({
  controllers: [BlogController],
  providers: [
    BlogService,
    AdminsDataAcceess,
    MediaDataAcceess,
    CategoryDataAccess,
    NewsCategoryDataAccess,
    NewsMediaDataAcceess,
    NewsDataAccess,
    PostDataAccess,
    PostMediaDataAcceess,
    ConvertDate,
    Tools,
    Jwt,
  ],
})
export class BlogModule {}
