import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import Promise = require('bluebird');
import { DepartmentDataAcceess } from '../../../dataAccess/department.dataAccess';
import { Tools } from '../../../common/helpers/tools.helper';
import { DepartmentDto, DepartmentObjDto } from '../../../DTO/Depatrment.dto';
import mainDepartments from '../../../common/eNums/mainDeparments.enum';

@Injectable()
export class BasicDefinitionService {
  constructor(
    private readonly departmentDataAcceess: DepartmentDataAcceess,
    private readonly tools: Tools,
  ) {}

  async createDepartment(createDepartment) {
    const { mainDeparmentId, title, description } = createDepartment;
    const maindepartment = mainDepartments.find(
      (element) => element.id === mainDeparmentId,
    );
    if (!maindepartment) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'mainDeparmentId invalid',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    try {
      await this.departmentDataAcceess.createdeparment(
        mainDeparmentId,
        title,
        description,
      );

      return;
    } catch (err) {
      throw err;
    }
  }
  async departments(mainDeparmentId): Promise<DepartmentDto[]> {
    const maindepartment = mainDepartments.find(
      (element) => element.id === mainDeparmentId,
    );
    if (!maindepartment) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'mainDeparmentId invalid',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const departments = await this.departmentDataAcceess.findByMainDeparmentId(
      mainDeparmentId,
    );
    return departments.map((department) => DepartmentObjDto(department));
  }
}
