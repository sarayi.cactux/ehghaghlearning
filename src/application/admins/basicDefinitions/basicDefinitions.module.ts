import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { BasicDefinitionController } from './basicDefinitions.controller';
import { BasicDefinitionService } from './basicDefinitions.service';
import { DepartmentDataAcceess } from '../../../dataAccess/department.dataAccess';
import { AdminsDataAcceess } from '../../../dataAccess/admins.dataAccess';
import { ValidAdminMiddleware } from '../../../common/middlewares/validateAdmin.middleware';
import { ConvertDate } from '../../../common/helpers/convertDate.helper';
import { Tools } from '../../../common/helpers/tools.helper';
import { Jwt } from '../../../common/helpers/jwt.helper';

@Module({
  controllers: [BasicDefinitionController],
  providers: [
    BasicDefinitionService,
    DepartmentDataAcceess,
    AdminsDataAcceess,
    ConvertDate,
    Tools,
    Jwt,
  ],
})
export class BasicDefinitionModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes({
      path: 'admin/basicDefinitions',
      method: RequestMethod.POST,
    });
  }
}
