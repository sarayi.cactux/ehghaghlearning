import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Response } from 'express';
import { BasicDefinitionService } from './basicDefinitions.service';
import { MainDepartmentDto } from '../../../DTO/mainDepatrment.dto';
import {
  CreateDepartmentDto,
  DepartmentDto,
} from '../../../DTO/Depatrment.dto';
import mainDepartments from '../../../common/eNums/mainDeparments.enum';

@ApiTags('basicDefinitions')
@Controller('admin/basicDefinitions')
export class BasicDefinitionController {
  constructor(
    private readonly basicDefinitionService: BasicDefinitionService,
  ) {}
  // get main deparments ***********************************************************************
  @ApiOkResponse({
    description: 'MainDepartmentDto',
    type: [MainDepartmentDto],
  })
  @Get('')
  @HttpCode(HttpStatus.OK)
  mainDepartments(): MainDepartmentDto[] {
    return mainDepartments;
  }
  // get secountDepartments by maindepartmentId   *******************
  @ApiOkResponse({
    description: 'secountDepartments',
    type: [DepartmentDto],
  })
  @Get('/:mainDepartmentId')
  @HttpCode(HttpStatus.OK)
  async secoundDepartments(
    @Res() res: Response,
    @Param(
      'mainDepartmentId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    mainDepartmentId: number,
  ): Promise<DepartmentDto[]> {
    try {
      const deparments = await this.basicDefinitionService.departments(
        mainDepartmentId,
      );
      res.status(HttpStatus.OK).json(deparments);
      return;
    } catch (err) {
      throw err;
    }
  }
  // creat new secound department *********************************************************************************
  @ApiOkResponse({
    description: 'DepartmentDto',
    type: [DepartmentDto],
  })
  @ApiBody({
    type: CreateDepartmentDto,
    description: 'create new Department',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createConsulting(
    @Res() res: Response,
    @Body() createDepartmentDto: CreateDepartmentDto,
  ): Promise<DepartmentDto[]> {
    try {
      await this.basicDefinitionService.createDepartment(createDepartmentDto);
      const deparments = await this.basicDefinitionService.departments(
        createDepartmentDto.mainDeparmentId,
      );
      res.status(HttpStatus.OK).json(deparments);
      return;
    } catch (err) {
      throw err;
    }
  }
}
