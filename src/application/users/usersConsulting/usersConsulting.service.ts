import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import Promise = require('bluebird');
import { UsersDataAcceess } from '../../../dataAccess/users.dataAccess';
import { ConsultingDataAcceess } from '../../../dataAccess/consulting.dataAccess';
import { ConsultingMessageDataAcceess } from '../../../dataAccess/consultingMessage.dataAccess';
import { ConsultingMessageMediaDataAcceess } from '../../../dataAccess/consultingMessageMedia.dataAccess';
import { MediaDataAcceess } from '../../../dataAccess/media.dataAccess';
import { Tools } from '../../../common/helpers/tools.helper';
import { UserDto, userObj } from 'src/DTO/user.dto';
import {
  ConsultingDto,
  consultingObjInUserDto,
} from '../../../DTO/consulting.dto';

@Injectable()
export class UsersConsultingService {
  constructor(
    private readonly usersDataAcceess: UsersDataAcceess,
    private readonly consultingDataAcceess: ConsultingDataAcceess,
    private readonly consultingMessageDataAcceess: ConsultingMessageDataAcceess,
    private readonly consultingMessageMediaDataAcceess: ConsultingMessageMediaDataAcceess,
    private readonly mediaDataAcceess: MediaDataAcceess,
    private readonly tools: Tools,
  ) {}

  async createConsulting(createConsultingDto, user): Promise<UserDto> {
    const {
      subject,
      consultingType,
      mainDeparment,
      secondDepartment,
      text,
      medias,
    } = createConsultingDto;
    try {
      const consulting = await this.consultingDataAcceess.create(
        user.id,
        subject,
        consultingType,
        mainDeparment,
        secondDepartment,
      );
      const consultingMessage = await this.consultingMessageDataAcceess.create(
        user.id,
        consulting.id,
        text,
      );

      if (
        typeof medias === 'object' &&
        medias.constructor === Array &&
        medias.length > 0
      ) {
        await Promise.map(medias, async (media) => {
          const item = await this.mediaDataAcceess.findById(media.mediaId);
          if (item) {
            this.consultingMessageMediaDataAcceess.create(
              media.mediaId,
              consultingMessage.id,
              media.title,
            );
          }
        });
      }
      const updatedUser = await this.usersDataAcceess.findById(user.id);
      return userObj(updatedUser, user.token);
    } catch (err) {
      throw err;
    }
  }
  async consultingDetail(consultingId, userId): Promise<ConsultingDto> {
    const consultingDetail = await this.consultingDataAcceess.find(
      consultingId,
      userId,
    );
    if (consultingDetail) {
      return consultingObjInUserDto(consultingDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  async createConsultingMessage(
    consultingId,
    userId,
    createConsultingMessageDto,
  ): Promise<ConsultingDto> {
    const consultingDetail = await this.consultingDataAcceess.find(
      consultingId,
      userId,
    );
    if (consultingDetail) {
      const consultingMessage = await this.consultingMessageDataAcceess.create(
        userId,
        consultingId,
        createConsultingMessageDto.text,
      );
      if (createConsultingMessageDto.medias) {
        await Promise.map(createConsultingMessageDto.medias, async (media) =>
          this.consultingMessageMediaDataAcceess.create(
            media.mediaId,
            consultingMessage.id,
            media.title,
          ),
        );
      }
      const newConsultingDetail = await this.consultingDataAcceess.find(
        consultingId,
        userId,
      );
      return consultingObjInUserDto(newConsultingDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
}
