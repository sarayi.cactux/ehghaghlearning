import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserConsultingModule } from './usersConsulting/usersconsulting.module';
import { UserTicketingModule } from './usersTicketing/usersTicket.module';
import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
import { MediaDataAcceess } from '../../dataAccess/media.dataAccess';
import {
  ValidUserMiddleware,
  CheckUserMiddleware,
  ValidateHeaderMiddleware,
} from '../../common/middlewares/validateUser.middleware';
import { Jwt } from '../../common/helpers/jwt.helper';
import { ConvertDate } from '../../common/helpers/convertDate.helper';
import { Tools } from '../../common/helpers/tools.helper';

@Module({
  imports: [UserConsultingModule, UserTicketingModule],
  controllers: [UsersController],
  providers: [
    UsersService,
    UsersDataAcceess,
    MediaDataAcceess,
    Jwt,
    ConvertDate,
    Tools,
  ],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidUserMiddleware).forRoutes(
      // '*',
      {
        path: 'users',
        method: RequestMethod.GET,
      },
    );
    consumer.apply(ValidateHeaderMiddleware).forRoutes(
      {
        path: 'users/uploadFile',
        method: RequestMethod.POST,
      },
      {
        path: 'users/:mediaId',
        method: RequestMethod.DELETE,
      },
    );
    consumer.apply(CheckUserMiddleware).forRoutes(
      {
        path: 'users/signIn',
        method: RequestMethod.POST,
      },
      {
        path: 'users/signIn',
        method: RequestMethod.PUT,
      },
    );
  }
}
