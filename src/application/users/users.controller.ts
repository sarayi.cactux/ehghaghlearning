import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Req,
  Res,
  Get,
  Put,
  UsePipes,
  UseInterceptors,
  UploadedFile,
  HttpException,
  Delete,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiHeader,
  ApiConsumes,
  ApiBody,
  ApiOperation,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { UsersService } from './users.service';
import {
  SignInDto,
  UserDto,
  AuthenticateDto,
  userObj,
} from '../../DTO/user.dto';
import { IpAddress } from '../../decorators/ipaddress.decorator';
import { MobilePipe } from '../../pipes/mobile.pipe';
import { MediaDto, mediaObj } from '../../DTO/media.dto';
import { ProCitiesDto } from '../../DTO/proCity.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName } from '../../common/middlewares/filename.middleware';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  @ApiOkResponse({
    description: 'user default info',
    type: [UserDto],
  })
  // user home page ********************************
  @Get('')
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @HttpCode(HttpStatus.OK)
  async index(@Req() req: Request, @Res() res: Response): Promise<UserDto> {
    const user = await this.usersService.userInf(res.locals.user);
    res.status(HttpStatus.OK).json(user);
    return;
  }
  // user signIn ****************************************************
  @Post('signIn')
  @UsePipes(new MobilePipe())
  @ApiOkResponse({
    description: 'token created',
    type: String,
  })
  @ApiNotFoundResponse({ description: 'user not found' })
  @ApiInternalServerErrorResponse({
    description: 'login faild',
  })
  @HttpCode(HttpStatus.OK)
  async signIn(
    @IpAddress() ip: string,
    @Res() res: Response,
    @Body() singIn: SignInDto,
  ): Promise<string> {
    try {
      if (res.locals && res.locals.user) {
        return res.locals.user;
      }
      const singInToken = await this.usersService.signIn(singIn.mobile, ip);
      res.status(HttpStatus.OK).json(singInToken);
      return;
    } catch (err) {
      throw err;
    }
  }
  @Put('signIn')
  @ApiHeader({
    name: 'singInToken',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    description: 'user info',
    type: [UserDto],
  })
  @HttpCode(HttpStatus.OK)
  async login(
    @IpAddress() ip: string,
    @Req() req: Request,
    @Res() res: Response,
    @Body() authenticateDto: AuthenticateDto,
  ): Promise<UserDto> {
    try {
      if (res.locals && res.locals.user) {
        res.status(HttpStatus.OK).json(res.locals.user);
        return;
      }
      const singInToken = req.header('singInToken');
      const { user, jwtToken } = await this.usersService.login(
        authenticateDto,
        singInToken,
        ip,
      );
      const userInf = userObj(user, jwtToken);
      req.session.user = userInf;
      res.status(HttpStatus.OK).json(userInf);
      return;
    } catch (err) {
      throw err;
    }
  }
  @Get('signOut')
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @ApiOkResponse({
    type: Boolean,
  })
  @HttpCode(HttpStatus.OK)
  async signOut(@Req() req: Request): Promise<boolean> {
    try {
      let token = '';
      if (req.header('ehghagh_jwt')) {
        token = req.header('ehghagh_jwt');
      }
      this.usersService.signOut(token);
      req.session.destroy;
      return true;
    } catch (err) {
      throw err;
    }
  }
  // upload file *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [MediaDto],
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @Post('/uploadFile')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/files',
        filename: editFileName,
      }),
    }),
  )
  async uploadMultipleFiles(@UploadedFile() file): Promise<any> {
    if (file) {
      const media = await this.usersService.upload(file.filename);
      return mediaObj(media);
    }
    throw new HttpException(
      {
        status: HttpStatus.BAD_REQUEST,
        error: 'BAD_REQUEST',
      },
      HttpStatus.BAD_REQUEST,
    );
  }
  // proCities Json *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [ProCitiesDto],
  })
  @Get('/proCities')
  @HttpCode(HttpStatus.OK)
  async proCities(): Promise<ProCitiesDto[]> {
    const proCities = await this.usersService.proCities();
    return proCities;
  }
  // delete uploaded media   *******************
  @ApiOperation({ summary: 'delete uploaded media' })
  @ApiNotFoundResponse({ description: 'file not found' })
  @ApiInternalServerErrorResponse({
    description: 'delete file faild',
  })
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'status',
    type: Boolean,
  })
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  @Delete('/:mediaId')
  @HttpCode(HttpStatus.OK)
  async secoundDepartments(
    @Param(
      'mediaId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    mediaId: number,
  ): Promise<boolean> {
    try {
      const status = await this.usersService.deleteMedia(mediaId);
      return status;
    } catch (err) {
      throw err;
    }
  }
}
