import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as moment from 'moment';
import * as fs from 'fs';
import { Jwt } from 'src/common/helpers/jwt.helper';
import { UsersDataAcceess } from 'src/dataAccess/users.dataAccess';
import { MediaDataAcceess } from 'src/dataAccess/media.dataAccess';
import { Tools } from '../../common/helpers/tools.helper';
import { userObj } from 'src/DTO/user.dto';
import { ProCitiesDto, proCitiesObj } from '../..//DTO/proCity.dto';

@Injectable()
export class UsersService {
  constructor(
    private readonly usersDataAcceess: UsersDataAcceess,
    private readonly mediaDataAcceess: MediaDataAcceess,
    private readonly jwt: Jwt,
    private readonly tools: Tools,
  ) {}
  async userInf(user) {
    const updatedUser = await this.usersDataAcceess.findById(user.id);
    return userObj(updatedUser, user.token);
  }
  async signIn(mobile: string, ip: string): Promise<any> {
    const smsRq = await this.usersDataAcceess.findSmsRq(mobile, ip);
    if (smsRq) {
      if (smsRq.sendCount > 4) {
        return smsRq.jwtToken;
      }
      const now = moment(new Date());
      const secondsDiff = now.diff(moment(smsRq.lastSentTime), 'seconds');
      if (secondsDiff < 120) {
        return smsRq.jwtToken;
      }
      let jwtToken = smsRq.jwtToken;
      if (mobile !== smsRq.mobile) {
        const tokenValues = {
          mobile: mobile,
        };
        jwtToken = this.jwt.signer(tokenValues);
      }
      const sendCount = smsRq.sendCount + 1;
      const code = this.tools.codeCreator();
      await this.tools.sendSmsCode(mobile, code);
      await this.usersDataAcceess.updateSmsRq(
        smsRq.id,
        jwtToken,
        mobile,
        sendCount,
        ip,
        code,
      );
      return jwtToken;
    } else {
      const tokenValues = {
        mobile: mobile,
      };
      const jwtToken = this.jwt.signer(tokenValues);
      const code = this.tools.codeCreator();
      await this.tools.sendSmsCode(mobile, code);
      await this.usersDataAcceess.createSmsRq(jwtToken, mobile, ip, code);
      return jwtToken;
    }
  }
  async login(authenticateDto, singInToken, ip) {
    const { code } = authenticateDto;
    const tokenValues = this.jwt.verifier(singInToken);
    if (tokenValues) {
      const mobile = tokenValues['mobile'];
      const smsRq = await this.usersDataAcceess.validateCode(
        mobile,
        code,
        singInToken,
      );
      if (smsRq) {
        let user = await this.usersDataAcceess.findByMobile(mobile);
        if (!user) {
          user = await this.usersDataAcceess.createUser(mobile, code);
        }
        const tokenValues = {
          userId: user.id,
        };
        const jwtToken = this.jwt.signer(tokenValues, 86400);
        await this.usersDataAcceess.createUserToken(
          user.id,
          ip,
          code,
          jwtToken,
        );
        this.usersDataAcceess.deleteSmsRq(smsRq.id);
        return { user, jwtToken };
      }
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'FORBIDDEN',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    throw new HttpException(
      {
        status: HttpStatus.REQUEST_TIMEOUT,
        error: 'REQUEST_TIMEOUT',
      },
      HttpStatus.REQUEST_TIMEOUT,
    );
  }
  async signOut(token) {
    this.usersDataAcceess.signOut(token);
    return;
  }
  async upload(filename) {
    const mimeType = await this.tools.checkMime(filename);
    const mediaUrl = filename;
    const file = await this.mediaDataAcceess.createMedia(mediaUrl, mimeType);
    return file;
  }
  async proCities(): Promise<ProCitiesDto[]> {
    const proCities = await this.usersDataAcceess.proCities();
    return proCities.map((pro) => proCitiesObj(pro));
  }
  async deleteMedia(mediaId) {
    const media = await this.mediaDataAcceess.findById(mediaId);
    if (media && media.ownerId === 0) {
      const fileUrl = `./public/files/${media.mediaUrl}`;
      fs.unlinkSync(fileUrl);
      await this.mediaDataAcceess.deleteMedia(mediaId);
      return true;
    }
    return false;
  }
}
