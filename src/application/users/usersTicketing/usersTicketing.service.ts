import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import Promise = require('bluebird');
import { TicketDataAcceess } from '../../../dataAccess/ticket.dataAccess';
import { TicketMessageDataAcceess } from '../../../dataAccess/ticketMessage.dataAccess';
import { TicketMessageMediaDataAcceess } from '../../../dataAccess/ticketMessageMedia.dataAccess';
import {
  TicketDto,
  ticketObjInUserDto,
  TicketListDto,
} from '../../../DTO/ticket.dto';
import departments from '../../../common/eNums/ticketDeparments.enum';
import priorities from '../../../common/eNums/ticketPriority.enum';

@Injectable()
export class UsersTicketService {
  constructor(
    private readonly ticketDataAcceess: TicketDataAcceess,
    private readonly ticketMessageDataAcceess: TicketMessageDataAcceess,
    private readonly ticketMessageMediaDataAcceess: TicketMessageMediaDataAcceess,
  ) {}

  async createTicket(createTicketDto, user) {
    const { subject, departmentId, priority, text, medias } = createTicketDto;
    try {
      const checkDepartment = departments.find((e) => e.code === departmentId);
      if (!checkDepartment) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'departmentId_NOT_ACCEPTABLE',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const checkPeriority = priorities.find((e) => e.code === priority);
      if (!checkPeriority) {
        throw new HttpException(
          {
            status: HttpStatus.NOT_ACCEPTABLE,
            error: 'priority_NOT_ACCEPTABLE',
          },
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const ticket = await this.ticketDataAcceess.create(
        user.id,
        subject,
        departmentId,
        priority,
      );
      const ticketMessage = await this.ticketMessageDataAcceess.create(
        user.id,
        ticket.id,
        text,
      );
      if (medias) {
        await Promise.map(medias, async (media) =>
          this.ticketMessageMediaDataAcceess.create(
            media.mediaId,
            ticketMessage.id,
            media.title,
          ),
        );
      }
      return true;
    } catch (err) {
      throw err;
    }
  }
  async ticketDetail(ticketId, userId): Promise<TicketDto> {
    const ticketDetail = await this.ticketDataAcceess.find(ticketId, userId);
    if (ticketDetail) {
      return ticketObjInUserDto(ticketDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  async createTicketMessage(
    ticketId,
    userId,
    createTicketMessageDto,
  ): Promise<TicketDto> {
    const ticketDetail = await this.ticketDataAcceess.find(ticketId, userId);
    if (ticketDetail) {
      const ticketMessage = await this.ticketMessageDataAcceess.create(
        userId,
        ticketId,
        createTicketMessageDto.text,
      );
      if (createTicketMessageDto.medias) {
        await Promise.map(createTicketMessageDto.medias, async (media) =>
          this.ticketMessageMediaDataAcceess.create(
            media.mediaId,
            ticketMessage.id,
            media.title,
          ),
        );
      }
      const newTicketDetail = await this.ticketDataAcceess.find(
        ticketId,
        userId,
      );
      return ticketObjInUserDto(newTicketDetail);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'NOT_FOUND',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
  async ticketsList(userId): Promise<TicketListDto[]> {
    const tickets = await this.ticketDataAcceess.findAllUserTickets(userId);
    const ticketList = tickets.map((ticket) => ticketObjInUserDto(ticket));
    return ticketList;
  }
}
