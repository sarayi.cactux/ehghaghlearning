import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { UsersTicketController } from './usersTicketing.controller';
import { UsersTicketService } from './usersTicketing.service';
import { UsersDataAcceess } from '../../../dataAccess/users.dataAccess';
import { TicketDataAcceess } from '../../../dataAccess/ticket.dataAccess';
import { TicketMessageDataAcceess } from '../../../dataAccess/ticketMessage.dataAccess';
import { TicketMessageMediaDataAcceess } from '../../../dataAccess/ticketMessageMedia.dataAccess';
import { ValidUserMiddleware } from '../../../common/middlewares/validateUser.middleware';
import { ConvertDate } from '../../../common/helpers/convertDate.helper';
import { Tools } from '../../../common/helpers/tools.helper';
import { Jwt } from '../../../common/helpers/jwt.helper';

@Module({
  controllers: [UsersTicketController],
  providers: [
    UsersTicketService,
    UsersDataAcceess,
    TicketDataAcceess,
    TicketMessageDataAcceess,
    TicketMessageMediaDataAcceess,
    ConvertDate,
    Tools,
    Jwt,
  ],
})
export class UserTicketingModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidUserMiddleware).forRoutes(
      'usersTicketing',
      // {
      //   path: 'usersTicketing',
      //   method: RequestMethod.POST,
      // },
      // {
      //   path: 'usersTicketing',
      //   method: RequestMethod.GET,
      // },
      // {
      //   path: 'usersTicketing/:ticketId',
      //   method: RequestMethod.GET,
      // },
      // {
      //   path: 'usersTicketing/:ticketId',
      //   method: RequestMethod.POST,
      // },
    );
  }
}
