import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Response } from 'express';
import { UsersTicketService } from './usersTicketing.service';
import { UserDto } from '../../../DTO/user.dto';
import {
  CreateTicketDto,
  TicketDto,
  TicketListDto,
} from '../../../DTO/ticket.dto';
import { CreateTicketMessageDto } from '../../../DTO/ticketMessage.dto';

@ApiTags('usersTicketing')
@Controller('usersTicketing')
export class UsersTicketController {
  constructor(private readonly usersTicketService: UsersTicketService) {}
  // get tickets *********************************************************************************
  @ApiOkResponse({
    description: 'user tickets list',
    type: [TicketListDto],
  })
  @Get('')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async getAllUserTickets(@Res() res: Response): Promise<TicketListDto[]> {
    try {
      const tickets = await this.usersTicketService.ticketsList(
        res.locals.user.id,
      );
      res.status(HttpStatus.OK).json({ tickets });
      return;
    } catch (err) {
      throw err;
    }
  }
  // creat new ticket *********************************************************************************
  @ApiOkResponse({
    description: 'creating new ticket status',
    type: Boolean,
  })
  @ApiBody({
    type: CreateTicketDto,
    description: 'create new Ticket',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createTicket(
    @Res() res: Response,
    @Body() createTicketDto: CreateTicketDto,
  ): Promise<UserDto> {
    try {
      const status = await this.usersTicketService.createTicket(
        createTicketDto,
        res.locals.user,
      );
      res.status(HttpStatus.OK).json(status);
      return;
    } catch (err) {
      throw err;
    }
  }

  // get ticket detail by id *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [TicketDto],
  })
  @Get('/:ticketId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async ticketDetail(
    @Res() res: Response,
    @Param(
      'ticketId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    ticketId: number,
  ): Promise<TicketDto> {
    try {
      const ticketDetail = await this.usersTicketService.ticketDetail(
        ticketId,
        res.locals.user.id,
      );
      res.status(HttpStatus.OK).json(ticketDetail);
      return;
    } catch (err) {
      throw err;
    }
  }

  // create new ticket message *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [TicketDto],
  })
  @Post('/:ticketId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createTicketMessage(
    @Res() res: Response,
    @Param(
      'ticketId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    ticketId: number,
    @Body() createTicketMessageDto: CreateTicketMessageDto,
  ): Promise<TicketDto> {
    try {
      const ticketDetail = await this.usersTicketService.createTicketMessage(
        ticketId,
        res.locals.user.id,
        createTicketMessageDto,
      );
      res.status(HttpStatus.OK).json(ticketDetail);
      return;
    } catch (err) {
      throw err;
    }
  }
}
