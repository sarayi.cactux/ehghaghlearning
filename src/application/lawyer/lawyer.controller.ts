import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Req,
  Res,
  Get,
  Put,
  UsePipes,
  UseInterceptors,
  UploadedFile,
  HttpException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiHeader,
  ApiConsumes,
  ApiBody,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { LawyerService } from './laywer.service';

import { CreatelawyerDto, LawyerDto } from '../../DTO/lawyerInf.dto';
import { NcodePipe } from '../../pipes/nCode.pipe';

@ApiTags('lawyers')
@Controller('lawyers')
export class LawyersController {
  constructor(private readonly lawyerService: LawyerService) {}
  // creat new consenting *********************************************************************************
  @ApiOkResponse({
    description: 'LawyerDto info',
    type: [LawyerDto],
  })
  @ApiBody({
    type: CreatelawyerDto,
    description: 'create new lawyer request',
  })
  @UsePipes(new NcodePipe())
  @Post('newRequest')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createLawyerRequest(
    @Res() res: Response,
    @Body() createlawyerDto: CreatelawyerDto,
  ): Promise<LawyerDto> {
    try {
      const lawyer = await this.lawyerService.createLawyerRequest(
        createlawyerDto,
        res.locals.user,
      );
      res.status(HttpStatus.OK).json(lawyer);
      return;
    } catch (err) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'INTERNAL_SERVER_ERROR',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
