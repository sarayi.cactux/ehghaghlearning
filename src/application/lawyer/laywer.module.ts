import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { LawyersController } from './lawyer.controller';
import { LawyerService } from './laywer.service';

import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
import { LawyersDataAcceess } from '../../dataAccess/lawyers.dataAccess';
import {
  ValidLawyererMiddleware,
  CheckLawyererMiddleware,
} from '../../common/middlewares/validateLawyer.middleware';
import { ValidUserMiddleware } from '../../common/middlewares/validateUser.middleware';
import { Jwt } from '../../common/helpers/jwt.helper';
import { ConvertDate } from '../../common/helpers/convertDate.helper';
import { Tools } from '../../common/helpers/tools.helper';

@Module({
  imports: [],
  controllers: [LawyersController],
  providers: [
    LawyerService,
    UsersDataAcceess,
    LawyersDataAcceess,
    Jwt,
    ConvertDate,
    Tools,
  ],
})
export class LawyerModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ValidLawyererMiddleware)
      .forRoutes
      // '*',
      // {
      //   path: 'users',
      //   method: RequestMethod.GET,
      // },
      // {
      //   path: 'users/uploadFile',
      //   method: RequestMethod.POST,
      // },
      ();
    consumer.apply(ValidUserMiddleware).forRoutes({
      path: 'lawyers/newRequest',
      method: RequestMethod.POST,
    });
  }
}
