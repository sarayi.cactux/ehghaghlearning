import { Injectable } from '@nestjs/common';
import { LawyersDataAcceess } from 'src/dataAccess/lawyers.dataAccess';
import { LawyerDto, lawyerObjFullDto } from 'src/DTO/lawyerInf.dto';

import { Tools } from '../../common/helpers/tools.helper';

@Injectable()
export class LawyerService {
  constructor(
    private readonly tools: Tools,
    private readonly lawyersDataAcceess: LawyersDataAcceess,
  ) {}
  async createLawyerRequest(createlawyerDto, user): Promise<LawyerDto> {
    // eslint-disable-next-line prettier/prettier
    const { nCode, name, family, proId, cityId, mediaId, sheba } =
      createlawyerDto;
    try {
      let lawyer = await this.lawyersDataAcceess.findByUserId(user.id);

      if (!lawyer) {
        await this.lawyersDataAcceess.createLawyerRequest(
          user.id,
          nCode,
          name,
          family,
          proId,
          cityId,
          mediaId,
          sheba,
        );
        lawyer = await this.lawyersDataAcceess.findByUserId(user.id);
      }

      return lawyerObjFullDto(lawyer);
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
