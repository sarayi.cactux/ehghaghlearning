import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { UsersConsultingController } from './usersConsulting.controller';
import { UsersConsultingService } from './usersconsulting.service';
import { UsersDataAcceess } from '../../../dataAccess/users.dataAccess';
import { ConsultingDataAcceess } from '../../../dataAccess/consulting.dataAccess';
import { ConsultingMessageDataAcceess } from '../../../dataAccess/consultingMessage.dataAccess';
import { ConsultingMessageMediaDataAcceess } from '../../../dataAccess/consultingMessageMedia.dataAccess';
import { ValidUserMiddleware } from '../../../common/middlewares/validateUser.middleware';
import { ConvertDate } from '../../../common/helpers/convertDate.helper';
import { Tools } from '../../../common/helpers/tools.helper';
import { Jwt } from '../../../common/helpers/jwt.helper';

@Module({
  controllers: [UsersConsultingController],
  providers: [
    UsersConsultingService,
    UsersDataAcceess,
    ConsultingDataAcceess,
    ConsultingMessageDataAcceess,
    ConsultingMessageMediaDataAcceess,
    ConvertDate,
    Tools,
    Jwt,
  ],
})
export class UserConsultingModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidUserMiddleware).forRoutes(
      {
        path: 'usersConsulting',
        method: RequestMethod.POST,
      },
      {
        path: 'usersConsulting/:consultingId',
        method: RequestMethod.GET,
      },
      {
        path: 'usersConsulting/:consultingId',
        method: RequestMethod.POST,
      },
    );
  }
}
