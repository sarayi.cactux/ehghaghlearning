import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Req,
  Res,
  Get,
  Put,
  HttpException,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiHeader,
  ApiBody,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { UsersConsultingService } from './usersconsulting.service';
import { UserDto } from '../../../DTO/user.dto';
import {
  CreateConsultingDto,
  ConsultingDto,
} from '../../../DTO/consulting.dto';
import { CreateConsultingMessageDto } from 'src/DTO/consultingMessage.dto';

@ApiTags('usersConsulting')
@Controller('usersConsulting')
export class UsersConsultingController {
  constructor(
    private readonly usersConsultingService: UsersConsultingService,
  ) {}
  // creat new consenting *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [UserDto],
  })
  @ApiBody({
    type: CreateConsultingDto,
    description: 'create new consulting',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createConsulting(
    @Res() res: Response,
    @Body() createConsultingDto: CreateConsultingDto,
  ): Promise<UserDto> {
    try {
      const user = await this.usersConsultingService.createConsulting(
        createConsultingDto,
        res.locals.user,
      );
      res.status(HttpStatus.OK).json(user);
      return;
    } catch (err) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'INTERNAL_SERVER_ERROR',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  // get consenting detail by id *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [ConsultingDto],
  })
  @Get('/:consultingId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async consultingDetail(
    @Res() res: Response,
    @Param(
      'consultingId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    consultingId: number,
  ): Promise<ConsultingDto> {
    try {
      const consultingDetail =
        await this.usersConsultingService.consultingDetail(
          consultingId,
          res.locals.user.id,
        );
      res.status(HttpStatus.OK).json({ consultingDetail });
      return;
    } catch (err) {
      throw err;
    }
  }

  // create new consulting message *********************************************************************************
  @ApiOkResponse({
    description: 'user default info',
    type: [ConsultingDto],
  })
  @Post('/:consultingId')
  @HttpCode(HttpStatus.OK)
  @ApiHeader({
    name: 'ehghagh_jwt',
    description: 'this token recieved in signIn',
  })
  async createConsultingMessage(
    @Res() res: Response,
    @Param(
      'consultingId',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }),
    )
    consultingId: number,
    @Body() createConsultingMessageDto: CreateConsultingMessageDto,
  ): Promise<ConsultingDto> {
    try {
      const consultingDetail =
        await this.usersConsultingService.createConsultingMessage(
          consultingId,
          res.locals.user.id,
          createConsultingMessageDto,
        );
      res.status(HttpStatus.OK).json({ consultingDetail });
      return;
    } catch (err) {
      throw err;
    }
  }
}
