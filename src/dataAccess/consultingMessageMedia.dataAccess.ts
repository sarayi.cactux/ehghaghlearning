import * as Models from '../models/index';

export class ConsultingMessageMediaDataAcceess {
  async create(mediaId, consultingMessageId, title) {
    await Models.ConsultingMessageMedia.create({
      mediaId,
      consultingMessageId,
      title,
    }).then((media) => {
      Models.Media.update(
        {
          ownerId: media.id,
        },
        {
          where: {
            id: mediaId,
            ownerModel: 'ConsultingMessage',
          },
        },
      );
    });
  }
}
