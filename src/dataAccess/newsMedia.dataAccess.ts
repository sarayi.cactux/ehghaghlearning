import * as Models from '../models/index';

export class NewsMediaDataAcceess {
  async create(mediaId, newsId, title) {
    const newsMedia = await Models.NewsMedia.create({
      mediaId,
      newsId,
      title,
    });
    return newsMedia;
  }
  async find(newsId, mediaId) {
    const newsMedia = await Models.NewsMedia.findOne({
      where: {
        newsId,
        mediaId,
      },
    });
    return newsMedia;
  }

  async findNewsMedia(id, newsId) {
    const newsMedia = await Models.NewsMedia.findOne({
      where: {
        id,
        newsId,
      },
    });
    return newsMedia;
  }

  async deleteNewsMedia(id) {
    await Models.NewsMedia.destroy({
      where: {
        id,
      },
    });
  }
}
