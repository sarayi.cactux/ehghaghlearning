import * as Models from '../models/index';

export class DepartmentDataAcceess {
  async findByMainDeparmentId(mainDeparmentId) {
    const departments = await Models.ConsultingDeparment.findAll({
      where: {
        mainDeparmentId,
      },
    });
    return departments;
  }

  async createdeparment(mainDeparmentId, title, description) {
    await Models.ConsultingDeparment.create({
      mainDeparmentId,
      title,
      description,
    });
  }
}
