import * as Models from '../models/index';

export class UsersDataAcceess {
  async findByMobile(mobile) {
    const user = await Models.User.findOne({
      where: {
        mobile,
      },
      include: [Models.Consulting, Models.UserToken],
    });
    return user;
  }
  async findById(id) {
    const user = await Models.User.findOne({
      where: {
        id,
      },
      include: [Models.Consulting, Models.UserToken],
    });
    return user;
  }
  async findUserToken(jwtToken) {
    const token = await Models.UserToken.findOne({
      where: {
        jwtToken,
      },
      include: [{ model: Models.User, include: [Models.Consulting] }],
    });

    return token;
  }
  async findSmsRq(mobile, ip) {
    const smsRq = await Models.SmsRq.findOne({
      where: {
        $or: {
          ip,
          mobile,
        },
      },
    });
    return smsRq;
  }
  async updateSmsRq(id, jwtToken, mobile, sendCount, ip, code) {
    await Models.SmsRq.update(
      {
        jwtToken,
        mobile,
        sendCount,
        ip,
        code,
        lastSentTime: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }
  async createSmsRq(jwtToken, mobile, ip, code) {
    await Models.SmsRq.create({
      jwtToken,
      mobile,
      sendCount: 1,
      ip,
      code,
      lastSentTime: new Date(),
    });
  }
  deleteSmsRqs() {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    Models.SmsRq.destroy({
      where: {
        lastSentTime: {
          $lte: date,
        },
      },
    });
  }
  deleteSmsRq(id) {
    Models.SmsRq.destroy({
      where: {
        id,
      },
    });
  }
  async validateCode(mobile, code, jwtToken) {
    const smsRq = await Models.SmsRq.findOne({
      where: {
        mobile,
        code,
        jwtToken,
      },
    });
    return smsRq;
  }
  async createUser(mobile, lastConfirmationId) {
    const user = await Models.User.create({
      mobile,
      isLawyer: false,
      isActive: true,
      lastConfirmationId,
    });

    return user;
  }
  async createUserToken(userId, ip, lastConfirmationId, jwtToken) {
    const userToken = await Models.UserToken.create({
      userId,
      ip,
      jwtToken,
      lastConfirmationId,
      device: '',
    });

    return userToken;
  }
  signOut(token) {
    Models.UserToken.destroy({ where: { jwtToken: token } });
  }
  async proCities() {
    const proCities = await Models.Province.findAll({
      where: {
        parent: 0,
      },
      include: [
        {
          model: Models.City,
          separate: true,
          order: [['id', 'ASC']],
        },
      ],
    });
    return proCities;
  }
}
