import * as Models from '../models/index';

export class TicketMessageMediaDataAcceess {
  async create(mediaId, ticketMessageId, title) {
    await Models.TicketMessageMedia.create({
      mediaId,
      ticketMessageId,
      title,
    }).then((media) => {
      Models.Media.update(
        {
          ownerId: media.id,
        },
        {
          where: {
            id: mediaId,
            ownerModel: 'TicketMessage',
          },
        },
      );
    });
  }
}
