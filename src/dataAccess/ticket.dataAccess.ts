import * as Models from '../models/index';

export class TicketDataAcceess {
  async create(userId, subject, departmentId, priority) {
    const ticket = await Models.Ticket.create({
      userId,
      subject,
      departmentId,
      priority,
      status: 1,
    });
    return ticket;
  }
  async find(id, userId) {
    const ticket = await Models.Ticket.findOne({
      where: {
        id,
        userId,
      },
      include: [
        {
          model: Models.TicketMessage,
          separate: true,
          order: [['id', 'ASC']],
          include: [
            { model: Models.TicketMessageMedia, include: [Models.Media] },
          ],
        },
      ],
    });
    return ticket;
  }
  async findAllUserTickets(userId) {
    const tickts = await Models.Ticket.findAll({
      where: {
        userId: userId,
      },
      order: [['id', 'DESC']],
    });
    return tickts;
  }
}
