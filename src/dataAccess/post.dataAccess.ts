import * as Models from '../models/index';

export class PostDataAccess {
  async checkDuplicate(title) {
    const post = await Models.Post.findOne({
      where: {
        slug: title,
      },
    });
    return post;
  }

  async createPostRequest(
    adminId,
    blogCatId,
    mediaId,
    title,
    slug,
    text,
    isInLanding,
  ) {
    const post = await Models.Post.create({
      blogCatId,
      adminId,
      mediaId,
      title,
      slug,
      text,
      isInLanding,
    });
    return post;
  }

  async updateSortPost(id) {
    await Models.Post.update(
      {
        sort: id,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async findById(id) {
    const post = await Models.Post.findOne({
      where: {
        id,
      },
      include: [
        Models.Media,
        Models.BlogCategory,
        Models.Admin,
        Models.PostComment,
        { model: Models.PostMedia, include: [Models.Media] },
      ],
    });
    return post;
  }

  // eslint-disable-next-line prettier/prettier
  async updatePost(id, adminId, blogCatId, mediaId, title, text, isInLanding) {
    await Models.Post.update(
      {
        adminId,
        blogCatId,
        mediaId,
        title,
        text,
        isInLanding,
        active: 0,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async changeStatusPost(id, active) {
    await Models.Post.update(
      {
        active,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async listAndFilter(filterListPostDto) {
    const where: any = {};
    const { blogCatId, title, visitCount, active, isInLanding } =
      filterListPostDto;
    if (title && title.length > 2) {
      where.title = { $like: `%${title}%` };
    }
    if (blogCatId) {
      where.blogCatId = blogCatId;
    }
    if (visitCount) {
      where.visitCount = visitCount;
    }
    if (active) {
      where.active = active;
    }
    if (isInLanding) {
      where.isInLanding = isInLanding;
    }
    const posts = await Models.Post.findAll({
      where,
      order: [['sort', 'DESC']],
    });
    return posts;
  }
  async changeSortPost(fromId, toId) {
    await Models.Post.findOne({
      where: {
        id: fromId,
      },
    }).then((fromRow) => {
      Models.Post.findOne({
        where: {
          id: toId,
        },
      }).then((toRow) => {
        const toSort = fromRow.sort;
        const toFrom = toRow.sort;
        Models.Post.update(
          {
            sort: toSort,
          },
          {
            where: {
              id: toId,
            },
          },
        ).then(() => {
          Models.Post.update(
            {
              sort: toFrom,
            },
            {
              where: {
                id: fromId,
              },
            },
          );
        });
      });
    });
  }
}
