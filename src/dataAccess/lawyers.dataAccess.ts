import * as Models from '../models/index';

export class LawyersDataAcceess {
  async findByUserId(userId) {
    const lawyer = await Models.LawyerInf.findOne({
      where: {
        userId,
      },
      include: [
        Models.User,
        Models.Media,
        Models.City,
        Models.Province,
        Models.LawyerExpertis,
      ],
    });
    return lawyer;
  }
  async findById(id) {
    const laywer = await Models.LawyerInf.findOne({
      where: {
        id,
      },
      include: [
        Models.User,
        Models.Media,
        Models.City,
        Models.Province,
        Models.LawyerExpertis,
      ],
    });
    return laywer;
  }
  async createLawyerRequest(
    userId,
    nCode,
    name,
    family,
    proId,
    cityId,
    mediaId,
    sheba,
  ) {
    const lawyer = await Models.LawyerInf.create({
      userId,
      nCode,
      name,
      family,
      proId,
      cityId,
      mediaId,
      sheba,
    });

    return lawyer;
  }
  async listAndFilter(filterlawyerDto) {
    const where: any = {};
    const { nCode, name, family, proId, cityId, isActive, acceptStatus } =
      filterlawyerDto;
    if (nCode && nCode.length > 5) {
      where.nCode = { $like: `%${nCode}%` };
    }
    if (name && name.length > 2) {
      where.name = { $like: `%${name}%` };
    }
    if (family && family.length > 2) {
      where.family = { $like: `%${family}%` };
    }
    if (proId) {
      where.proId = proId;
    }
    if (cityId) {
      where.cityId = cityId;
    }
    if (isActive) {
      where.isActive = isActive;
    }
    if (acceptStatus) {
      where.acceptStatus = acceptStatus;
    }
    const lawyers = await Models.LawyerInf.findAll({
      where,
      include: [Models.User, Models.City, Models.Province],
    });
    return lawyers;
  }
  async updateAcceptStatus(id, acceptStatus, rejectDescription) {
    await Models.LawyerInf.update(
      {
        acceptStatus,
        rejectDescription,
      },
      {
        where: {
          id,
        },
      },
    );
  }
}
