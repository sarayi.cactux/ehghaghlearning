import * as Models from '../models/index';

export class CategoryDataAccess {
  async checkDuplicate(title) {
    const category = await Models.BlogCategory.findOne({
      where: {
        title: title,
      },
    });
    return category;
  }

  async createCategory(title, description) {
    const category = await Models.BlogCategory.create({
      title,
      description,
    });
    return category;
  }

  async findAllBlogCategries() {
    const categories = await Models.BlogCategory.findAll({
      order: [['id', 'DESC']],
    });
    return categories;
  }

  async findById(id) {
    const category = await Models.BlogCategory.findOne({
      where: {
        id,
      },
    });
    return category;
  }
  async updateBlogCategory(id, title, description) {
    await Models.BlogCategory.update(
      {
        title,
        description,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async changeStatusCategory(id, active) {
    await Models.BlogCategory.update(
      {
        active,
      },
      {
        where: {
          id,
        },
      },
    );
  }
}
