import * as Models from '../models/index';

export class NewsCategoryDataAccess {
  async checkDuplicate(title) {
    const category = await Models.NewsCategory.findOne({
      where: {
        title: title,
      },
    });
    return category;
  }

  async crateNewsCategory(title, description) {
    const category = await Models.NewsCategory.create({
      title,
      description,
    });
    return category;
  }

  async findAll() {
    const newsCategories = await Models.NewsCategory.findAll({
      order: [['id', 'DESC']],
    });
    return newsCategories;
  }

  async findById(id) {
    const category = await Models.NewsCategory.findOne({
      where: {
        id,
      },
    });
    return category;
  }

  async updateNewsCategory(id, title, description) {
    await Models.NewsCategory.update(
      {
        title,
        description,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async changeStatusNewsCategory(id, active) {
    await Models.NewsCategory.update(
      {
        active,
      },
      {
        where: {
          id,
        },
      },
    );
  }
}
