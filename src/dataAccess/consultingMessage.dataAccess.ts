import * as Models from '../models/index';

export class ConsultingMessageDataAcceess {
  async create(userId, consultingId, text) {
    const consulting = await Models.ConsultingMessage.create({
      userId,
      consultingId,
      text,
      isNextFree: true,
      payAmount: 0,
      isPayed: false,
    });
    return consulting;
  }
}
