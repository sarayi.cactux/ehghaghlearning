import * as Models from '../models/index';

export class ConsultingDataAcceess {
  async create(
    userId,
    subject,
    consultingType,
    mainDeparment,
    secondDepartment,
  ) {
    const consulting = await Models.Consulting.create({
      userId,
      subject,
      consultingType,
      mainDeparment,
      secondDepartment,
      status: 1,
    });
    return consulting;
  }
  async find(id, userId) {
    const consulting = await Models.Consulting.findOne({
      where: {
        id,
        userId,
      },
      include: [
        {
          model: Models.ConsultingMessage,
          separate: true,
          order: [['id', 'ASC']],
          include: [
            { model: Models.ConsultingMessageMedia, include: [Models.Media] },
          ],
        },
      ],
    });
    return consulting;
  }
}
