import * as Models from '../models/index';

export class AdminsDataAcceess {
  async findByUserName(userName) {
    const admin = await Models.Admin.findOne({
      where: {
        userName,
      },
    });
    return admin;
  }
  async findById(id) {
    const admin = await Models.Admin.findByPk(id);
    return admin;
  }
  async updateJwtToken(jwtToken, id) {
    await Models.Admin.update(
      {
        jwtToken,
      },
      {
        where: {
          id,
        },
      },
    );
  }
  async updatePassword(id, newPassword) {
    await Models.Admin.update(
      {
        password: newPassword,
      },
      {
        where: {
          id,
        },
      },
    );
  }
}
