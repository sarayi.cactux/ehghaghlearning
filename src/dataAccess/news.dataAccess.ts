import * as Models from '../models/index';

export class NewsDataAccess {
  async checkDuplicate(title) {
    const news = await Models.News.findOne({
      where: {
        slug: title,
      },
    });
    return news;
  }

  async createNewsRequest(
    adminId,
    newsCatId,
    mediaId,
    title,
    slug,
    text,
    source,
    sourceLink,
    isInLanding,
  ) {
    const news = await Models.News.create({
      adminId,
      newsCatId,
      mediaId,
      title,
      slug,
      text,
      source,
      sourceLink,
      isInLanding,
    });
    return news;
  }

  async updateSortNews(id) {
    await Models.News.update(
      {
        sort: id,
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async findById(id) {
    const news = await Models.News.findOne({
      where: {
        id,
      },
      include: [
        Models.Media,
        Models.NewsCategory,
        Models.Admin,
        Models.NewsComment,
        { model: Models.NewsMedia, include: [Models.Media] },
      ],
    });
    return news;
  }

  // eslint-disable-next-line prettier/prettier
  async updateNews(
    id,
    adminId,
    newsCatId,
    mediaId,
    title,
    text,
    source,
    sourceLink,
    isInLanding,
  ) {
    await Models.News.update(
      {
        adminId,
        newsCatId,
        mediaId,
        title,
        text,
        source,
        sourceLink,
        isInLanding,
        active: 0,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async changeStatusNews(id, active) {
    await Models.News.update(
      {
        active,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
  }

  async listAndFilter(filterListNewsDto) {
    const where: any = {};
    const { newsCatId, title, visitCount, active, isInLanding } =
      filterListNewsDto;
    if (title && title.length > 2) {
      where.title = { $like: `%${title}%` };
    }
    if (newsCatId) {
      where.blogCatId = newsCatId;
    }
    if (visitCount) {
      where.visitCount = visitCount;
    }
    if (active) {
      where.active = active;
    }
    if (isInLanding) {
      where.isInLanding = isInLanding;
    }
    const news = await Models.News.findAll({
      where,
      order: [['sort', 'DESC']],
    });
    return news;
  }

  async changeSortNews(fromId, toId) {
    await Models.News.findOne({
      where: {
        id: fromId,
      },
    }).then((fromRow) => {
      Models.News.findOne({
        where: {
          id: toId,
        },
      }).then((toRow) => {
        const toSort = fromRow.sort;
        const toFrom = toRow.sort;
        Models.News.update(
          {
            sort: toSort,
          },
          {
            where: {
              id: toId,
            },
          },
        ).then(() => {
          Models.News.update(
            {
              sort: toFrom,
            },
            {
              where: {
                id: fromId,
              },
            },
          );
        });
      });
    });
  }
}
