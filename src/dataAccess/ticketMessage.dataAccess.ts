import * as Models from '../models/index';

export class TicketMessageDataAcceess {
  async create(userId, ticketId, text) {
    const ticketMessaage = await Models.TicketMessage.create({
      userId,
      ticketId,
      text,
    });
    return ticketMessaage;
  }
}
