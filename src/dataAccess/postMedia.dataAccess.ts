import * as Models from '../models/index';

export class PostMediaDataAcceess {
  async create(mediaId, postId, title) {
    const postMedia = await Models.PostMedia.create({
      mediaId,
      postId,
      title,
    });
    return postMedia;
  }
  async find(postId, mediaId) {
    const postMedia = await Models.PostMedia.findOne({
      where: {
        postId,
        mediaId,
      },
    });
    return postMedia;
  }

  async findPostMedia(id, postId) {
    const postMedia = await Models.PostMedia.findOne({
      where: {
        id,
        postId,
      },
    });
    return postMedia;
  }

  async deletePostMedia(id) {
    await Models.PostMedia.destroy({
      where: {
        id,
      },
    });
  }
}
