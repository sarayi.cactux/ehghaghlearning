import { Module } from '@nestjs/common';
import { ThrottlerModule } from '@nestjs/throttler';
import sequilzeObj from './database/sequilze.obj';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

import { AdminModule } from './application/admins/admins.module';
import { UserModule } from './application/users/users.module';
import { LawyerModule } from './application/lawyer/laywer.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    sequilzeObj,
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    AdminModule,
    UserModule,
    LawyerModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
