import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import blogStatuses from '../common/eNums/blogStatuses.enum ';
import { PostDto, postObjDto } from './post.dto';

export function blogCatObjDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  let Posts = [];
  if (category.Posts && category.Posts.length !== 0) {
    Posts = category.Posts.map((post) => postObjDto(post));
  }
  return {
    id: category.id,
    title: category.title,
    description: category.description,
    activeText: blogStatuses[active].text,
    active: category.acive,
    createdAt: category.createdAt,
    Posts,
  };
}

export function blogCatObjInfoDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  return {
    title: category.title,
    activeText: blogStatuses[active].text,
  };
}

export class BlogCatDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ type: PostDto, isArray: true }) Posts: PostDto[];
}

export class BlogCatInfoDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;
}

export class BlogCatListDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  active: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class CreateBlogCatDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;
}

export class UpdateBlogCatDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsOptional()
  @ApiProperty({ type: String })
  title: string;

  @IsOptional()
  @ApiProperty({ type: String })
  description: string;
}

export class StatusBlogCatDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  active: number;
}
