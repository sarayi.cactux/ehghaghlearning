import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { mediaObj, MediaDto } from './media.dto';

export function newsMediaObj(newsMedia) {
  return {
    id: newsMedia.id,
    mediaId: newsMedia.mediaId,
    title: newsMedia.title,
    mediaObj: mediaObj(newsMedia.Media),
  };
}

export class NewsMediaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({
    type: MediaDto,
  })
  mediaObj: MediaDto;
}

export class CreateNewsMediaDto {
  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;
}

export class DeleteNewsMediaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  newsId: number;
}
