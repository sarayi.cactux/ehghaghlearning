import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { ConvertDate } from '../common/helpers/convertDate.helper';
import {
  consultingPaymentObjInUserDto,
  ConsultingPaymentInUserDto,
} from './consultingPayment.dto';
import {
  consultingMessageMediaObj,
  ConsultingMessageMadiaDto,
  createConsultingMessageMadiaDto,
} from './consultingMessageMedia.dto';
const convertDate = new ConvertDate();

export function consultingMessageObjInUserDto(consultingMessage) {
  let consultingPayment = {};
  if (consultingMessage.ConsultingPayment) {
    consultingPayment = consultingPaymentObjInUserDto(
      consultingMessage.ConsultingPayment,
    );
  }
  let consultingMessageMedias = [];
  if (
    consultingMessage.ConsultingMessageMedias &&
    consultingMessage.ConsultingMessageMedias.length !== 0
  ) {
    consultingMessageMedias = consultingMessage.ConsultingMessageMedias.map(
      (media) => consultingMessageMediaObj(media),
    );
  }

  return {
    id: consultingMessage.id,
    userId: consultingMessage.userId,
    text: consultingMessage.text,
    isNextFree: consultingMessage.isNextFree,
    payAmount: consultingMessage.payAmount,
    isPayed: consultingMessage.isPayed,
    createdAt: convertDate.jalaliDateWithTime(consultingMessage.createdAt),
    consultingPayment,
    consultingMessageMedias,
  };
}
export class ConsultingMessageDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: Boolean })
  isNextFree: boolean;

  @ApiProperty({ type: Number })
  payAmount: number;

  @ApiProperty({ type: Boolean })
  isPayed: boolean;

  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ type: ConsultingPaymentInUserDto })
  consultingPayment: ConsultingPaymentInUserDto;

  @ApiProperty({
    isArray: true,
    type: ConsultingMessageMadiaDto,
  })
  ConsultingMessageMedias: ConsultingMessageMadiaDto[];
}
export class CreateConsultingMessageDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ isArray: true, type: createConsultingMessageMadiaDto })
  medias: createConsultingMessageMadiaDto[];
}
