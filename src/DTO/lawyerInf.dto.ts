import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsIn,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
} from 'class-validator';
import { ConvertDate } from '../common/helpers/convertDate.helper';

import lawyerStatuses from '../common/eNums/lawyerStatuses.enum';

import { userObjLowDetail, UserLowDto } from './user.dto';
import { MediaDto, mediaObj } from './media.dto';

const convertDate = new ConvertDate();
export function lawyerObjFullDto(lawyer) {
  const user = userObjLowDetail(lawyer.User);
  const media = mediaObj(lawyer.Media);
  const status = Object.keys(lawyerStatuses).find(
    (key) => lawyerStatuses[key].code === lawyer.acceptStatus,
  );
  return {
    id: lawyer.id,
    userId: lawyer.userId,
    nCode: lawyer.nCode,
    name: lawyer.name,
    family: lawyer.family,
    proId: lawyer.proId,
    pro: lawyer.Province.title,
    cityId: lawyer.cityId,
    city: lawyer.City.title,
    sheba: lawyer.sheba,
    isActive: lawyer.isActive,
    status: lawyerStatuses[status],
    acceptStatus: lawyer.acceptStatus,
    rejectDescription: lawyer.rejectDescription,
    createdAt: convertDate.jalaliDateWithTime(lawyer.createdAt),
    mediaObj: media,
    user,
    // LawyerExpertises: lawyer.LawyerExpertises,
  };
}
export function lawyerListObj(lawyer) {
  const status = Object.keys(lawyerStatuses).find(
    (key) => lawyerStatuses[key].code === lawyer.acceptStatus,
  );
  return {
    id: lawyer.id,
    userId: lawyer.userId,
    nCode: lawyer.nCode,
    name: lawyer.name,
    family: lawyer.family,
    proId: lawyer.proId,
    pro: lawyer.Province.title,
    cityId: lawyer.cityId,
    city: lawyer.City.title,
    isActive: lawyer.isActive,
    status: lawyerStatuses[status],
    acceptStatus: lawyer.acceptStatus,
    createdAt: convertDate.jalaliDateWithTime(lawyer.createdAt),
  };
}

export class LawyerDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  nCode: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  family: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  proId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  pro: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  cityId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  city: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  sheba: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isActive: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  status: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  acceptStatus: number;

  @ApiProperty({ type: String })
  rejectDescription: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({
    type: MediaDto,
  })
  mediaObj: MediaDto;

  @ApiProperty({
    type: UserLowDto,
  })
  user: UserLowDto;

  // @ApiProperty({ isArray: true, type: LawyerExpertiseDto })
  // LawyerExpertises: LawyerExpertiseDto[];
}

export class CreatelawyerDto {
  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: String })
  nCode: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  family: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  proId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  cityId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNumberString()
  @ApiProperty({ type: String })
  sheba: string;
}

export class LawyerListDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  nCode: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  family: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  pro: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  city: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  status: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isActive: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}
export class FilterlawyerDto {
  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  nCode: number;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  name: string;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  family: string;

  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  proId: number;

  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  cityId: number;

  @IsOptional()
  @ApiProperty({ type: Boolean, required: false })
  isActive: boolean;

  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  acceptStatus: number;
}
export class UpdateAcceptStatusLawyerDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  acceptStatus: number;

  @IsOptional()
  @ApiProperty({ type: String })
  rejectDescription: string;
}
