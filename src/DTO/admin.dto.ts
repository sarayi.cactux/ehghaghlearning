import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export function adminObj(admin, token) {
  return {
    userName: admin.userName,
    name: admin.name,
    lastName: admin.lastName,
    mobile: admin.mobile,
    email: admin.email,
    token,
  };
}
export function adminObjDto(admin) {
  return {
    userName: admin.userName,
    name: admin.name,
    lastName: admin.lastName,
  };
}
export class LoginDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  password: string;
}
export class UpdatePasswordDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  oldPassword: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  newPassword: string;
}
export class AdminDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  lastName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @IsEmail()
  @ApiProperty({ type: String })
  email: string;

  @ApiProperty({ type: String })
  token: string;
}

export class AdminInfoDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  lastName: string;
}
