import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsNumberString, IsOptional } from 'class-validator';
import blogStatuses from '../common/eNums/blogStatuses.enum ';
import {
  NewsMediaDto,
  newsMediaObj,
  CreateNewsMediaDto,
} from './newsMedia.dto';
import { NewsCommentDto, NewsCommentObj } from './newsComment.dto';
import { MediaDto, mediaObj } from './media.dto';
import { newsCatInfoDto, newsCatObjInfoDto } from './newsCategory.dto';
import { adminObjDto, AdminInfoDto } from './admin.dto';
import { Optional } from '@nestjs/common';

export function newsObjDto(news) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === news.active,
  );
  let media = null;
  if (news.Media) {
    media = mediaObj(news.Media);
  }
  return {
    id: news.id,
    adminId: news.adminId,
    newsCatId: news.newsCatId,
    mediaId: news.mediaId,
    media,
    title: news.title,
    slug: news.slug,
    text: news.text,
    visitCount: news.visitCount,
    sort: news.sort,
    source: news.source,
    sourceLink: news.sourceLink,
    isInLanding: news.isInLanding,
    activeText: blogStatuses[status].text,
    active: news.active,
    createdAt: news.createdAt,
  };
}

export function newsInfoObjDto(news) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === news.active,
  );

  let NewsMedias = [];
  if (news.NewsMedias && news.NewsMedias.length !== 0) {
    NewsMedias = news.NewsMedias.map((newsMedia) => newsMediaObj(newsMedia));
  }
  let NewsComments = [];
  if (news.NewsComments && news.NewsComments.length !== 0) {
    NewsComments = news.NewsComments.map((newsComment) =>
      NewsCommentObj(newsComment),
    );
  }
  const newsCat = newsCatObjInfoDto(news.NewsCategory);
  const admin = adminObjDto(news.Admin);
  let media = null;
  if (news.Media) {
    media = mediaObj(news.Media);
  }
  return {
    id: news.id,
    adminId: news.adminId,
    admin,
    newsCatId: news.newsCatId,
    newsCat,
    mediaId: news.mediaId,
    media,
    title: news.title,
    slug: news.slug,
    text: news.text,
    visitCount: news.visitCount,
    sort: news.sort,
    source: news.source,
    sourceLink: news.sourceLink,
    isInLanding: news.isInLanding,
    activeText: blogStatuses[status].text,
    active: news.active,
    createdAt: news.createdAt,
    NewsMedias,
    NewsComments,
  };
}

export class NewsDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  newsCatId: number;

  @ApiProperty({ type: () => newsCatInfoDto })
  newsCat: newsCatInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  adminId: number;

  @ApiProperty({ type: AdminInfoDto })
  admin: AdminInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @ApiProperty({ type: MediaDto })
  media: MediaDto;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @ApiProperty({ type: Number })
  sort: number;

  @ApiProperty({ type: String })
  source: string;

  @ApiProperty({ type: String })
  sourceLink: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ isArray: true, type: NewsMediaDto })
  NewsMedias: NewsMediaDto[];

  @ApiProperty({ isArray: true, type: NewsCommentDto })
  NewsComments: NewsCommentDto[];
}

export class ListNewsDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  newsCatId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  adminId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  source: string;

  @ApiProperty({ type: String })
  sourceLink: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class FilterListNewsDto {
  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  newsCatId: number;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  title: string;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  visitCount: number;

  @IsOptional()
  @ApiProperty({ type: Boolean, required: false })
  isInLanding: boolean;

  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  active: number;
}

export class CreateNewsDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  newsCatId: number;

  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  source: string;

  @ApiProperty({ type: String })
  sourceLink: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @Optional()
  @ApiProperty({ isArray: true, type: CreateNewsMediaDto })
  newsMedias: CreateNewsMediaDto[];
}

export class UpdateNewsDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  newsCatId: number;

  @IsInt()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  source: string;

  @ApiProperty({ type: String })
  sourceLink: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @Optional()
  @ApiProperty({ isArray: true, type: CreateNewsMediaDto })
  newsMedias: CreateNewsMediaDto[];
}

export class NewsStatusDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  active: number;
}

export class ChangeSortNewsDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  fromId: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  toId: number;
}
