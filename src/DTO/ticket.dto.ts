import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { ConvertDate } from '../common/helpers/convertDate.helper';
import ticketStatuses from '../common/eNums/ticketStatuses.enum';
import departments from '../common/eNums/ticketDeparments.enum';
import priorities from '../common/eNums/ticketPriority.enum';
import {
  ticketMessageObjInUserDto,
  TicketMessageDto,
} from './ticketMessage.dto';
import { createticketMessageMadiaDto } from './ticketMessageMedia.dto';

const convertDate = new ConvertDate();
export function ticketObjInUserDto(ticket) {
  const status = Object.keys(ticketStatuses).find(
    (key) => ticketStatuses[key].code === ticket.status,
  );
  const priority = priorities.find((e) => e.code === ticket.priority).text;
  const department = departments.find(
    (e) => e.code === ticket.departmentId,
  ).title;
  let ticketMessages = [];
  if (ticket.TicketMessages && ticket.TicketMessages.length !== 0) {
    ticketMessages = ticket.TicketMessages.map((message) =>
      ticketMessageObjInUserDto(message),
    );
  }
  return {
    id: ticket.id,
    userId: ticket.userId,
    subject: ticket.subject,
    departmentId: ticket.departmentId,
    status: ticketStatuses[status].text,
    statusCode: ticket.status,
    priority,
    department,
    createdAt: convertDate.jalaliDateWithTime(ticket.createdAt),
    ticketMessages,
  };
}

export class TicketDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  subject: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  status: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  statusCode: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  department: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  priority: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ isArray: true, type: TicketMessageDto })
  ticketMessages: TicketMessageDto[];
}

export class CreateTicketDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  subject: string;

  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: Number })
  departmentId: number;

  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: Number })
  priority: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ isArray: true, type: createticketMessageMadiaDto })
  medias: createticketMessageMadiaDto[];
}
export class TicketListDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  subject: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  status: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  department: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  priority: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  statusCode: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  departmentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}
