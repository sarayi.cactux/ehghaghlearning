import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import mainDepartments from '../common/eNums/mainDeparments.enum';

export class CreateDepartmentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mainDeparmentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;
}
export class DepartmentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mainDeparmentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mainDeparmentTitle: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;
}
export function DepartmentObjDto(depatment) {
  const maindepartment = mainDepartments.find(
    (element) => element.id === depatment.mainDeparmentId,
  );
  return {
    id: depatment.id,
    mainDeparmentTitle: maindepartment.title,
    mainDeparmentId: depatment.mainDeparmentId,
    title: depatment.title,
  };
}
