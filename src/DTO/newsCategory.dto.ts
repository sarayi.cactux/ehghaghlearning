import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import blogStatuses from '../common/eNums/blogStatuses.enum ';
//import { ConvertDate } from '../common/helpers/convertDate.helper';

export function newsCatObjDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  return {
    id: category.id,
    title: category.title,
    description: category.description,
    active: blogStatuses[active].text,
    createdAt: category.createdAt,
  };
}

export function newsCatObjInfoDto(category) {
  const active = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === category.active,
  );
  return {
    title: category.title,
    activeText: blogStatuses[active].text,
  };
}

export class NewsCatDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class CreateNewsCatDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;
}

export class UpdateNewsCatDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsOptional()
  @ApiProperty({ type: String })
  title: string;

  @IsOptional()
  @ApiProperty({ type: String })
  description: string;
}

export class StatusNewsCatDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  active: number;
}

export class NewsCatListDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  active: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class newsCatInfoDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;
}
