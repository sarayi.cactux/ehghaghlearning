import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { mediaObj, MediaDto } from './media.dto';

export function ticketMessageMediaObj(ticketMessageMadia) {
  return {
    id: ticketMessageMadia.id,
    mediaId: ticketMessageMadia.mediaId,
    title: ticketMessageMadia.title,
    mediaObj: mediaObj(ticketMessageMadia.Media),
  };
}
export class ticketMessageMadiaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({
    type: MediaDto,
  })
  mediaObj: MediaDto;
}

export class createticketMessageMadiaDto {
  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;
}
