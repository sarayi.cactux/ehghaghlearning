import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { ConvertDate } from '../common/helpers/convertDate.helper';

const convertDate = new ConvertDate();

export function consultingPaymentObjInUserDto(consultingPayment) {
  return {
    id: consultingPayment.userName,
    amount: consultingPayment.amount,
    traceNumber: consultingPayment.traceNumber,
    rnn: consultingPayment.rnn,
    datePaied: consultingPayment.datePaied,
    createdAt: convertDate.jalaliDateWithTime(consultingPayment.createdAt),
  };
}

export class ConsultingPaymentInUserDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  amount: number;

  @ApiProperty({ type: String })
  rnn: string;

  @ApiProperty({ type: String })
  datePaied: string;

  @ApiProperty({ type: String })
  createdAt: string;
}
