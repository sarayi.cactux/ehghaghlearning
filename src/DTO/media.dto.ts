import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export function mediaObj(media) {
  return {
    id: media.id,
    mediaUrl: media.mediaUrl,
    ownerId: media.ownerId,
    mimeType: media.mimeType,
  };
}
export class MediaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mediaUrl: string;

  @ApiProperty({ type: Number })
  ownerId: number;

  @ApiProperty({ type: String })
  mimeType: string;
}
