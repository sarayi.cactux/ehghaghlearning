import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { mediaObj, MediaDto } from './media.dto';

export function consultingMessageMediaObj(consultingMessageMadia) {
  return {
    id: consultingMessageMadia.id,
    mediaId: consultingMessageMadia.mediaId,
    title: consultingMessageMadia.title,
    mediaObj: mediaObj(consultingMessageMadia.Media),
  };
}
export class ConsultingMessageMadiaDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({
    type: MediaDto,
  })
  mediaObj: MediaDto;
}

export class createConsultingMessageMadiaDto {
  @IsNotEmpty()
  @IsNumberString()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;
}
