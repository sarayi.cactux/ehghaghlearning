import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsNumberString, IsOptional } from 'class-validator';
import blogStatuses from '../common/eNums/blogStatuses.enum ';
import {
  PostMediaDto,
  postMediaObj,
  CreatePostMediaDto,
} from './postMedia.dto';
import { PostCommentDto, postCommentObj } from './postComment.dto';
import { MediaDto, mediaObj } from './media.dto';
import { BlogCatInfoDto, blogCatObjInfoDto } from './blogCategory.dto';
import { adminObjDto, AdminInfoDto } from './admin.dto';
import { Optional } from '@nestjs/common';

export function postObjDto(post) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === post.active,
  );
  let media = null;
  if (post.Media) {
    media = mediaObj(post.Media);
  }
  return {
    id: post.id,
    adminId: post.adminId,
    blogCatId: post.blogCatId,
    mediaId: post.mediaId,
    media,
    title: post.title,
    slug: post.slug,
    text: post.text,
    visitCount: post.visitCount,
    sort: post.sort,
    isInLanding: post.isInLanding,
    activeText: blogStatuses[status].text,
    active: post.active,
    createdAt: post.createdAt,
  };
}

export function postInfoObjDto(post) {
  const status = Object.keys(blogStatuses).find(
    (key) => blogStatuses[key].code === post.active,
  );

  let PostMedias = [];
  if (post.PostMedias && post.PostMedias.length !== 0) {
    PostMedias = post.PostMedias.map((postMedia) => postMediaObj(postMedia));
  }
  let PostComments = [];
  if (post.PostComments && post.PostComments.length !== 0) {
    PostComments = post.PostComments.map((postComment) =>
      postCommentObj(postComment),
    );
  }
  const blogCat = blogCatObjInfoDto(post.BlogCategory);
  const admin = adminObjDto(post.Admin);
  let media = null;
  if (post.Media) {
    media = mediaObj(post.Media);
  }
  return {
    id: post.id,
    adminId: post.adminId,
    admin,
    blogCatId: post.blogCatId,
    blogCat,
    mediaId: post.mediaId,
    media,
    title: post.title,
    slug: post.slug,
    text: post.text,
    visitCount: post.visitCount,
    sort: post.sort,
    isInLanding: post.isInLanding,
    activeText: blogStatuses[status].text,
    active: post.active,
    createdAt: post.createdAt,
    PostMedias,
    PostComments,
  };
}

export class PostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @ApiProperty({ type: () => BlogCatInfoDto })
  blogCat: BlogCatInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  adminId: number;

  @ApiProperty({ type: AdminInfoDto })
  admin: AdminInfoDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @ApiProperty({ type: MediaDto })
  media: MediaDto;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  activeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @ApiProperty({ type: Number })
  sort: number;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({ isArray: true, type: PostMediaDto })
  PostMedias: PostMediaDto[];

  @ApiProperty({ isArray: true, type: PostCommentDto })
  PostComments: PostCommentDto[];
}
export class ListPostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  adminId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  visitCount: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  active: number;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;
}

export class FilterListPostDto {
  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  blogCatId: number;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  title: string;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  visitCount: number;

  @IsOptional()
  @ApiProperty({ type: Boolean, required: false })
  isInLanding: boolean;

  @IsOptional()
  @IsNumberString()
  @ApiProperty({ type: Number, required: false })
  active: number;
}

export class CreatePostDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @Optional()
  @ApiProperty({ isArray: true, type: CreatePostMediaDto })
  postMedias: CreatePostMediaDto[];
}

export class UpdatePostDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  blogCatId: number;

  @IsInt()
  @ApiProperty({ type: Number })
  mediaId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isInLanding: boolean;

  @Optional()
  @ApiProperty({ isArray: true, type: CreatePostMediaDto })
  postMedias: CreatePostMediaDto[];
}

export class PostStatusDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  active: number;
}

export class ChangeSortPostDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  fromId: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  toId: number;
}
