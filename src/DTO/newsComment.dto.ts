import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { CreatedAt } from 'sequelize-typescript';

export function NewsCommentObj(newsComment) {
  return {
    id: newsComment.id,
    newsId: newsComment.postId,
    parentId: newsComment.parentId,
    userName: newsComment.userName,
    active: newsComment.active,
    CreatedAt: newsComment.CreatedAt,
  };
}

export class NewsCommentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  newsId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  active: string;
}
