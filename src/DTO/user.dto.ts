import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { consultingObjInUserDto, ConsultingDto } from './consulting.dto';

export function userObj(user, token) {
  let Consultings = [];
  if (user.Consultings && user.Consultings.length > 0) {
    Consultings = user.Consultings.map((consulting) =>
      consultingObjInUserDto(consulting),
    );
  }
  return {
    id: user.id,
    mobile: user.mobile,
    isLawyer: user.isLawyer,
    isActive: user.isActive,
    token,
    Consultings,
  };
}
export function userObjLowDetail(user) {
  return {
    id: user.id,
    mobile: user.mobile,
    isLawyer: user.isLawyer,
  };
}
export class UserLowDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @ApiProperty({ type: Boolean })
  isLawyer: boolean;
}
export class SignInDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;
}
export class AuthenticateDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ type: Number })
  code: number;
}
export class UserDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @ApiProperty({ type: Boolean })
  isLawyer: boolean;

  @ApiProperty({ type: String })
  token: string;

  @ApiProperty({ isArray: true, type: ConsultingDto })
  Consultings: ConsultingDto[];
}
