import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { ConvertDate } from '../common/helpers/convertDate.helper';
import {
  ticketMessageMediaObj,
  ticketMessageMadiaDto,
  createticketMessageMadiaDto,
} from './ticketMessageMedia.dto';
const convertDate = new ConvertDate();

export function ticketMessageObjInUserDto(ticketMessage) {
  let ticketMessageMedias = [];
  if (
    ticketMessage.TicketMessageMedias &&
    ticketMessage.TicketMessageMedias.length !== 0
  ) {
    ticketMessageMedias = ticketMessage.TicketMessageMedias.map((media) =>
      ticketMessageMediaObj(media),
    );
  }

  return {
    id: ticketMessage.id,
    userId: ticketMessage.userId,
    text: ticketMessage.text,
    payAmount: ticketMessage.payAmount,
    isPayed: ticketMessage.isPayed,
    createdAt: convertDate.jalaliDateWithTime(ticketMessage.createdAt),
    ticketMessageMedias,
  };
}
export class TicketMessageDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  ticketId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ type: String })
  createdAt: string;

  @ApiProperty({
    isArray: true,
    type: ticketMessageMadiaDto,
  })
  ticketMessageMedias: ticketMessageMadiaDto[];
}
export class CreateTicketMessageDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ isArray: true, type: createticketMessageMadiaDto })
  medias: createticketMessageMadiaDto[];
}
