import { ApiProperty } from '@nestjs/swagger';

export function cityObj(city) {
  return {
    id: city.id,
    title: city.title,
    parent: city.parent,
  };
}
export class CityDto {
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: Number })
  parent: number;
}
