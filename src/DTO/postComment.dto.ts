import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumberString } from 'class-validator';
import { CreatedAt } from 'sequelize-typescript';

export function postCommentObj(postComment) {
  return {
    id: postComment.id,
    postId: postComment.postId,
    parentId: postComment.parentId,
    userName: postComment.userName,
    active: postComment.active,
    CreatedAt: postComment.CreatedAt,
  };
}

export class PostCommentDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  postId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  parentId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  active: string;
}
