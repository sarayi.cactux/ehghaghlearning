import { ApiProperty } from '@nestjs/swagger';
import { CityDto, cityObj } from './city.dto';

export function proCitiesObj(province) {
  const Cities = province.Cities.map((city) => cityObj(city));
  return {
    id: province.id,
    title: province.title,
    Cities,
  };
}
export class ProCitiesDto {
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ isArray: true, type: CityDto })
  Cities: CityDto[];
}
