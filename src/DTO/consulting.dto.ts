import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsInt } from 'class-validator';
import { ConvertDate } from '../common/helpers/convertDate.helper';
import cunsultingStatuses from '../common/eNums/cunsultingStatuses.enum';
import {
  consultingPaymentObjInUserDto,
  ConsultingPaymentInUserDto,
} from './consultingPayment.dto';
import {
  consultingMessageObjInUserDto,
  ConsultingMessageDto,
} from './consultingMessage.dto';
import { createConsultingMessageMadiaDto } from './consultingMessageMedia.dto';

const convertDate = new ConvertDate();
export function consultingObjInUserDto(consulting) {
  const status = Object.keys(cunsultingStatuses).find(
    (key) => cunsultingStatuses[key].code === consulting.status,
  );
  let ConsultingPayments = [];
  if (
    consulting.ConsultingPayments &&
    consulting.ConsultingPayments.length !== 0
  ) {
    ConsultingPayments = consulting.ConsultingPayments.map((payment) =>
      consultingPaymentObjInUserDto(payment),
    );
  }
  let ConsultingMessages = [];
  if (
    consulting.ConsultingMessages &&
    consulting.ConsultingMessages.length !== 0
  ) {
    ConsultingMessages = consulting.ConsultingMessages.map((message) =>
      consultingMessageObjInUserDto(message),
    );
  }
  return {
    id: consulting.id,
    userId: consulting.userId,
    subject: consulting.subject,
    consultingType: consulting.consultingType,
    status: cunsultingStatuses[status].text,
    statusCode: consulting.status,
    mainDeparment: consulting.mainDeparment,
    secondDepartment: consulting.secondDepartment,
    createdAt: convertDate.jalaliDateWithTime(consulting.createdAt),
    updatedAt: convertDate.jalaliDateWithTime(consulting.updatedAt),
    ConsultingPayments,
    ConsultingMessages,
  };
}

export class ConsultingDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  subject: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  status: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  statusCode: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  mainDeparment: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  secondDepartment: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  createdAt: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  updatedAt: string;

  @ApiProperty({ isArray: true, type: ConsultingPaymentInUserDto })
  ConsultingPayments: ConsultingPaymentInUserDto[];

  @ApiProperty({ isArray: true, type: ConsultingMessageDto })
  ConsultingMessages: ConsultingMessageDto[];
}

export class CreateConsultingDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  subject: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  consultingType: string;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  mainDeparment: number;

  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  secondDepartment: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  text: string;

  @ApiProperty({ isArray: true, type: createConsultingMessageMadiaDto })
  medias: createConsultingMessageMadiaDto[];
}
