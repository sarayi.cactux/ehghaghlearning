'use strict';

module.exports = {
  up: (queryInterface) =>
    queryInterface.bulkInsert(
      'admins',
      [
        {
          userName: 'admin',
          password: '81dc9bdb52d04dc20036dbd8313ed055',
          name: 'admin',
          lastName: 'admin',
          mobile: '01',
          email: 'sfsdfsffs@ss.co',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {
        fields: ['userName'],
        ignoreDuplicates: true,
      },
    ),
  down: (queryInterface) => queryInterface.bulkDelete('admin', null, {}),
};
