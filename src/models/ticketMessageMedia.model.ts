import {
  Column,
  Table,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Media } from './media.model';
import { TicketMessage } from './ticketMessage.model';

@Table({
  tableName: 'ticket_message_medias',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class TicketMessageMedia extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @ForeignKey(() => TicketMessage)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'ticket_messages', key: 'id' },
  })
  ticketMessageId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @BelongsTo(() => TicketMessage, { foreignKey: 'ticketMessageId' })
  TicketMessage: TicketMessage;
}
