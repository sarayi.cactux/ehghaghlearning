import {
  Column,
  Table,
  Model,
  HasMany,
  HasOne,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { ConsultingPayment } from './consultingPayment.model';
import { ConsultingMessageMedia } from './consultingMessageMedia.model';
import { User } from './user.model';
import { Consulting } from './consulting.model';

@Table({
  tableName: 'consulting_messages',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class ConsultingMessage extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @ForeignKey(() => Consulting)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'consultings', key: 'id' },
  })
  consultingId: number;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  text: string;

  @Column({
    allowNull: false,
    defaultValue: true,
    type: Sequelize.BOOLEAN,
  })
  isNextFree: boolean;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  payAmount: number;

  @Column({
    allowNull: false,
    defaultValue: false,
    type: Sequelize.BOOLEAN,
  })
  isPayed: boolean;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasOne(() => ConsultingPayment, { foreignKey: 'consultingMessageId' })
  ConsultingPayment: ConsultingPayment[];

  @HasMany(() => ConsultingMessageMedia, { foreignKey: 'consultingMessageId' })
  ConsultingMessageMedias: ConsultingMessageMedia[];

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => Consulting, { foreignKey: 'consultingId' })
  Consulting: Consulting;
}
