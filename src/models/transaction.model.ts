import {
  Column,
  Table,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';
import { Media } from './media.model';

@Table({
  tableName: 'transactions',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Transaction extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  lawyerId: number;

  @Column({
    allowNull: false,
    type: Sequelize.DATE,
  })
  requestDate: Date;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  amount: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    defaultValue: 0,
  })
  status: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  statusMessage: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  traceNumber: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  rnn: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  datePaied: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  digitalReceipt: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  issuerBank: string;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  payAmount: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

 @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'lawyerId' })
  User: User;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;
}
