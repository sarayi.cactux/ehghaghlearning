import {
  Column,
  Table,
  Model,
  HasMany,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';
import { Media } from './media.model';
import { City } from './city.model';
import { Province } from './province.model';
import { LawyerExpertis } from './lawyerExpertise.model';
import lawyerStatuses from '../common/eNums/lawyerStatuses.enum';

@Table({
  tableName: 'lawyers_inf',
  paranoid: true,
  deletedAt: 'deletedAt',
  omitNull: true,
})
export class LawyerInf extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
    allowNull: false,
  })
  userId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  nCode: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  name: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  family: string;

  @ForeignKey(() => Province)
  @Column({
    allowNull: false,
    references: { model: 'province_city', key: 'id' },
    type: Sequelize.INTEGER,
  })
  proId: number;

  @ForeignKey(() => City)
  @Column({
    allowNull: false,
    references: { model: 'province_city', key: 'id' },
    type: Sequelize.INTEGER,
  })
  cityId: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: true,
    references: { model: 'medias', key: 'id' },
    type: Sequelize.INTEGER,
  })
  mediaId: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  sheba: string;

  @Column({
    allowNull: true,
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  })
  isActive: boolean;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    defaultValue: lawyerStatuses.newRequest.code,
  })
  acceptStatus: number;

  @Column({
    allowNull: true,
    type: Sequelize.TEXT,
  })
  rejectDescription: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @BelongsTo(() => City, { foreignKey: 'cityId' })
  City: City;

  @BelongsTo(() => Province, { foreignKey: 'proId' })
  Province: Province;

  @HasMany(() => LawyerExpertis, { foreignKey: 'lawyers_inf' })
  LawyerExpertises: LawyerExpertis[];
}
