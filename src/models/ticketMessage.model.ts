import {
  Column,
  Table,
  Model,
  HasMany,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { TicketMessageMedia } from './ticketMessageMedia.model';
import { User } from './user.model';
import { Ticket } from './ticket.model';

@Table({
  tableName: 'ticket_messages',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class TicketMessage extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @ForeignKey(() => Ticket)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'tickets', key: 'id' },
  })
  ticketId: number;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  text: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => TicketMessageMedia, { foreignKey: 'ticketMessageId' })
  TicketMessageMedias: TicketMessageMedia[];

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => Ticket, { foreignKey: 'ticketId' })
  Ticket: Ticket;
}
