import { Column, Table, Model, IsEmail, HasMany } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Post } from './post.model';
import { News } from './news.model';

@Table({
  tableName: 'admins',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Admin extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    unique: true,
    allowNull: false,
    type: Sequelize.STRING,
  })
  userName: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  password: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  name: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  lastName: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  mobile: string;

  @Column({
    allowNull: true,
    type: Sequelize.TEXT,
  })
  jwtToken: string;

  @IsEmail
  @Column({
    unique: true,
    type: Sequelize.STRING,
    allowNull: false,
  })
  email: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => Post, { foreignKey: 'adminId' })
  Posts: Post[];

  @HasMany(() => News, { foreignKey: 'adminId' })
  Newses: News[];
}
