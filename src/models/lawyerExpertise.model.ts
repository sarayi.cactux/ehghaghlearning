import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { LawyerInf } from './lawyerInf.model';

@Table({
  tableName: 'lawyers_expertise',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class LawyerExpertis extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => LawyerInf)
  @Column({
    type: Sequelize.INTEGER,
    references: { model: 'lawyers_inf', key: 'id' },
    allowNull: false,
  })
  LawyerInfId: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  expertiseId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => LawyerInf, { foreignKey: 'lawyerInfId' })
  LawyerInf: LawyerInf;
}
