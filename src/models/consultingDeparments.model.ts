import { Column, Table, Model, HasMany } from 'sequelize-typescript';
import { Consulting } from './consulting.model';

import Sequelize from 'sequelize';

@Table({
  tableName: 'consulting_deparments',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class ConsultingDeparment extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  mainDeparmentId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    allowNull: true,
    type: Sequelize.TEXT,
  })
  description: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => Consulting, { foreignKey: 'secondDepartment' })
  Consultings: Consulting[];
}
