import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { News } from './news.model';
import { Media } from './media.model';

@Table({
  tableName: 'news_medias',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class NewsMedia extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  title: string;

  @ForeignKey(() => News)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'news', key: 'id' },
  })
  newsId: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @BelongsTo(() => News, { foreignKey: 'newsId' })
  News: News;
}
