import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Media } from './media.model';
import { ConsultingMessage } from './consultingMessage.model';

@Table({
  tableName: 'consulting_message_medias',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class ConsultingMessageMedia extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @ForeignKey(() => ConsultingMessage)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'consulting_messages', key: 'id' },
  })
  consultingMessageId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @BelongsTo(() => ConsultingMessage, { foreignKey: 'consultingMessageId' })
  ConsultingMessage: ConsultingMessage;
}
