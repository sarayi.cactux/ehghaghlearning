import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { NewsCategory } from './newsCategory.model';
import { Admin } from './admin.model';
import { Media } from './media.model';
import { NewsMedia } from './newsMedia.model';
import { NewsComment } from './newsComment.model';

@Table({
  tableName: 'news',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class News extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => NewsCategory)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'news_categories', key: 'id' },
  })
  newsCatId: number;

  @ForeignKey(() => Admin)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'admins', key: 'id' },
  })
  adminId: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  slug: string;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  text: string;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  visitCount: number;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  active: number;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  sort: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  source: string;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  sourceLink: string;

  @Column({
    defaultValue: false,
    allowNull: false,
    type: Sequelize.BOOLEAN,
  })
  isInLanding: boolean;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Admin, { foreignKey: 'adminId' })
  Admin: Admin;

  @BelongsTo(() => NewsCategory, { foreignKey: 'newsCatId' })
  NewsCategory: NewsCategory;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @HasMany(() => NewsComment, { foreignKey: 'newsId' })
  NewsComments: NewsComment[];

  @HasMany(() => NewsMedia, { foreignKey: 'newsId' })
  NewsMedias: NewsMedia[];
}
