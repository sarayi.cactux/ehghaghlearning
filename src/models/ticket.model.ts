import {
  Column,
  Table,
  Model,
  HasMany,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';
import { TicketMessage } from './ticketMessage.model';

@Table({
  tableName: 'tickets',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Ticket extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  departmentId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  subject: string;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  priority: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    defaultValue: 0,
  })
  status: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => TicketMessage, { foreignKey: 'ticketId' })
  TicketMessages: TicketMessage[];

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;
}
