import {
  Column,
  Table,
  Model,
  HasMany,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { ConsultingMessage } from './consultingMessage.model';
import { ConsultingPayment } from './consultingPayment.model';
import { ConsultingDeparment } from './consultingDeparments.model';
import { User } from './user.model';

@Table({
  tableName: 'consultings',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Consulting extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  subject: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  consultingType: string;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  status: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  mainDeparment: number;

  @ForeignKey(() => Consulting)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'consulting_deparments', key: 'id' },
  })
  secondDepartment: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => ConsultingPayment, { foreignKey: 'consultingId' })
  ConsultingPayments: ConsultingPayment[];

  @HasMany(() => ConsultingMessage, { foreignKey: 'consultingId' })
  ConsultingMessages: ConsultingMessage[];

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => ConsultingDeparment, { foreignKey: 'secondDepartment' })
  ConsultingDeparment: ConsultingDeparment;
}
