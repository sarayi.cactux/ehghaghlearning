import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';

@Table({
  tableName: 'lawyer_comments',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class LawyerComment extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  lawyerId: number;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  text: string;

  @Column({
    allowNull: false,
    type: Sequelize.BOOLEAN,
  })
  status: boolean;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => User, { foreignKey: 'lawyerId' })
  Lawyer: User;
}
