import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { News } from './news.model';

@Table({
  tableName: 'news_comments',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class NewsComment extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => News)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'news', key: 'id' },
  })
  newsId: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  parentId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  userName: string;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  active: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => News, { foreignKey: 'newsId' })
  News: News;

  @HasMany(() => NewsComment, { foreignKey: 'parentId' })
  NewsComments: NewsComment[];
}
