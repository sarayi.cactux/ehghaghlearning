import {
  Column,
  Table,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';
import { Consulting } from './consulting.model';
import { ConsultingMessage } from './consultingMessage.model';

@Table({
  tableName: 'consulting_payments',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class ConsultingPayment extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  lawyerId: number;

  @ForeignKey(() => Consulting)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'consultings', key: 'id' },
  })
  consultingId: number;

  @ForeignKey(() => ConsultingMessage)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'consulting_messages', key: 'id' },
  })
  consultingMessageId: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  amount: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  traceNumber: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  rnn: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  datePaied: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  digitalReceipt: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  issuerBank: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => User, { foreignKey: 'lawyerId' })
  Lawyer: User;

  @BelongsTo(() => Consulting, { foreignKey: 'consultingId' })
  Consulting: Consulting;

  @BelongsTo(() => ConsultingMessage, { foreignKey: 'consultingMessageId' })
  ConsultingMessage: ConsultingMessage;
}
