import { Column, Table, Model } from 'sequelize-typescript';
import Sequelize from 'sequelize';

@Table({
  tableName: 'sms_rqs',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class SmsRq extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  mobile: string;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  code: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  ip: string;

  @Column({
    allowNull: false,
    type: Sequelize.DATE,
  })
  lastSentTime: Date;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    defaultValue: 1,
  })
  sendCount: number;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  jwtToken: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;
}
