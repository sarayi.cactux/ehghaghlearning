import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Province } from './province.model';
import { LawyerInf } from './lawyerInf.model';

@Table({
  tableName: 'province_city',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class City extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => Province)
  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  })
  parent: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Province)
  Province: Province;

  @HasMany(() => LawyerInf, { foreignKey: 'cityId' })
  LawyerInfs: LawyerInf[];
}
