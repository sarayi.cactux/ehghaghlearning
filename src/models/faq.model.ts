import { Column, Table, Model } from 'sequelize-typescript';
import Sequelize from 'sequelize';

@Table({
  tableName: 'faqs',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Faq extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    unique: true,
    allowNull: false,
    type: Sequelize.STRING,
  })
  question: string;

  @Column({
    allowNull: false,
    type: Sequelize.TEXT,
  })
  answer: string;

  @Column({
    allowNull: false,
    type: Sequelize.BOOLEAN,
  })
  isLawyer: boolean;

 @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  // @HasMany(() => Book)
  // books: Book[];
}
