import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Post } from './post.model';

@Table({
  tableName: 'post_comments',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class PostComment extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => Post)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'posts', key: 'id' },
  })
  postId: number;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  parentId: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  userName: string;

  @Column({
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  active: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Post, { foreignKey: 'postId' })
  Post: Post;

  @HasMany(() => PostComment, { foreignKey: 'parentId' })
  PostComments: PostComment[];
}
