import {
  Column,
  Table,
  Model,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { User } from './user.model';

@Table({
  tableName: 'user_tokens',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class UserToken extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  device: string;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
  })
  ip: string;

  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  lastConfirmationId: number;

  @Column({
    allowNull: true,
    type: Sequelize.TEXT,
  })
  jwtToken: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;
}
