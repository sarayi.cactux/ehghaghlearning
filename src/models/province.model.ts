import { Column, Table, Model, HasMany } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { City } from './city.model';
import { LawyerInf } from './lawyerInf.model';

@Table({
  tableName: 'province_city',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Province extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0,
  })
  parent: number;

  @Column({
    allowNull: true,
    type: Sequelize.STRING,
  })
  title: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => City)
  Cities: City[];

  @HasMany(() => LawyerInf)
  LawyerInfs: LawyerInf[];
}
