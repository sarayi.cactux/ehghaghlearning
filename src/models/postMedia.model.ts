import {
  Column,
  Table,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Post } from './post.model';
import { Media } from './media.model';

@Table({
  tableName: 'post_medias',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class PostMedia extends Model {
  static delete(arg0: { where: { id: any } }) {
    throw new Error('Method not implemented.');
  }
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  title: string;

  @ForeignKey(() => Post)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'posts', key: 'id' },
  })
  postId: number;

  @ForeignKey(() => Media)
  @Column({
    allowNull: false,
    type: Sequelize.INTEGER,
    references: { model: 'medias', key: 'id' },
  })
  mediaId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Media, { foreignKey: 'mediaId' })
  Media: Media;

  @BelongsTo(() => Post, { foreignKey: 'postId' })
  Post: Post;
}
