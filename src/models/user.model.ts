import {
  Column,
  Table,
  Model,
  HasMany,
  ForeignKey,
} from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Transaction } from './transaction.model';
import { Ticket } from './ticket.model';
import { TicketMessage } from './ticketMessage.model';
import { LawyerComment } from './lawyerComment.model';
import { ConsultingPayment } from './consultingPayment.model';
import { ConsultingMessage } from './consultingMessage.model';
import { Consulting } from './consulting.model';
import { UserToken } from './userToken.model';

@Table({
  tableName: 'users',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class User extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  })
  mobile: string;

  @ForeignKey(() => User)
  @Column({
    allowNull: false,
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  })
  isLawyer: boolean;

  @Column({
    allowNull: false,
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  })
  isActive: boolean;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  lastConfirmationId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => Transaction, { foreignKey: 'lawyerId' })
  Transactions: Transaction[];

  @HasMany(() => UserToken, { foreignKey: 'userId' })
  UserTokens: UserToken[];

  @HasMany(() => Ticket, { foreignKey: 'userId' })
  Tickets: Ticket[];

  @HasMany(() => TicketMessage, { foreignKey: 'userId' })
  TicketMessages: TicketMessage[];

  @HasMany(() => LawyerComment, { foreignKey: 'lawyerId' })
  LawyerComments: LawyerComment[];

  @HasMany(() => ConsultingPayment, { foreignKey: 'userId' })
  ConsultingPaymentUsers: ConsultingPayment[];

  @HasMany(() => ConsultingPayment, { foreignKey: 'lawyerId' })
  ConsultingPaymentLawyers: ConsultingPayment[];

  @HasMany(() => ConsultingMessage, { foreignKey: 'userId' })
  ConsultingMessages: ConsultingMessage[];

  @HasMany(() => Consulting, { foreignKey: 'userId' })
  Consultings: Consulting[];
}
