const mainDepartments = [
  {
    id: 1,
    title: 'خانواده',
    description: 'خانواده',
  },
  {
    id: 2,
    title: 'ارث و وصایا',
    description: 'ارث و وصایا',
  },
  {
    id: 3,
    title: 'قراردادها - عقود',
    description: '',
  },
  {
    id: 4,
    title: 'کیفری',
    description: 'کیفری',
  },
  {
    id: 5,
    title: 'اسناد تجاری',
    description: 'اسناد تجاری',
  },
  {
    id: 6,
    title: 'حقوق بیمه',
    description: 'حقوق بیمه',
  },
  {
    id: 7,
    title: 'جرایم رایانه ای',
    description: 'جرایم رایانه ای',
  },
  {
    id: 8,
    title: 'حقوقی',
    description: 'حقوقی',
  },
  {
    id: 9,
    title: 'شهرداری',
    description: 'شهرداری',
  },
  {
    id: 10,
    title: 'مالیات',
    description: 'مالیات',
  },
  {
    id: 11,
    title: 'املاک و مستغلات',
    description: '',
  },
  {
    id: 12,
    title: 'کار و کارگری',
    description: 'کار و کارگری',
  },
  {
    id: 13,
    title: 'مهاجرت',
    description: 'مهاجرت',
  },
  {
    id: 14,
    title: 'شرکت ها',
    description: 'شرکت ها',
  },
  {
    id: 15,
    title: 'موارد دیگر',
    description: 'موارد دیگر',
  },
];
export default mainDepartments;
