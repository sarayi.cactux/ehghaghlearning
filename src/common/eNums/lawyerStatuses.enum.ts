const lawyerStatuses = {
  newRequest: {
    code: 1,
    text: 'در انتظار بررسی',
  },
  pending: {
    code: 2,
    text: 'در حال بررسی',
  },
  accepted: {
    code: 3,
    text: 'تایید شده',
  },
  rejected: {
    code: 4,
    text: 'درخواست رد شده',
  },
};
export default lawyerStatuses;
