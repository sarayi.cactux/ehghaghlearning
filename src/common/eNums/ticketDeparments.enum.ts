const departments = [
  {
    code: 1,
    title: 'فنی',
  },
  {
    code: 2,
    title: 'مالی',
  },
  {
    code: 3,
    title: 'شکایات',
  },
  {
    code: 4,
    title: 'پیشنهادات',
  },
  {
    code: 5,
    title: 'سایر',
  },
];
export default departments;
