const ticketStatuses = {
  newRequest: {
    code: 1,
    text: 'در انتظار بررسی',
  },
  pending: {
    code: 2,
    text: 'در حال بررسی',
  },
  answered: {
    code: 3,
    text: 'پاسخ داده شده',
  },
  customerAnswer: {
    code: 4,
    text: 'پاسخ مشتری',
  },
  closed: {
    code: 5,
    text: 'بسته شده',
  },
};
export default ticketStatuses;
