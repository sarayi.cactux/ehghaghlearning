const mimeTypes = [
  ['doc', 'xls', 'pdf'],
  ['jpg', 'png', 'gif', 'jpeg'],
  ['mp3'],
  ['mp4'],
];
export default mimeTypes;
