const cunsultingTypes = [
  {
    title: 'مشاوره حقوقی',
    description: 'مشاوره حقوقی',
  },
  {
    title: 'درخواست تنظیم اوراق حقوقی',
    description:
      'درخواست تنظیم دادخواست، شکایت نامه، قرارداد، دادنامه، لایحه دفاعیه و ...',
  },
];
export default cunsultingTypes;
