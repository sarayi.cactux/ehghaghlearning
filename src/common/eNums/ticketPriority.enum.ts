const ticketPeriority = [
  {
    code: 1,
    text: 'زیاد',
  },
  {
    code: 2,
    text: 'متوسط',
  },
  {
    code: 3,
    text: 'کم',
  },
];
export default ticketPeriority;
