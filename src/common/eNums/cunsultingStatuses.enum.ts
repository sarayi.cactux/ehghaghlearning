const cunsultingStatuses = {
  newRequest: {
    code: 1,
    text: 'در انتظار بررسی',
  },
  pending: {
    code: 2,
    text: 'در حال بررسی',
  },
  answered: {
    code: 3,
    text: 'پاسخ داده شده',
  },
  awaitingPayment: {
    code: 4,
    text: 'در انتظار پرداخت',
  },
  paid: {
    code: 5,
    text: 'پرداخت شده',
  },
  customerAnswer: {
    code: 6,
    text: 'پاسخ مشتری',
  },
  closed: {
    code: 7,
    text: 'بسته شده',
  },
};
export default cunsultingStatuses;
