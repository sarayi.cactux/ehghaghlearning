import {
  Injectable,
  NestMiddleware,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { Jwt } from '../helpers/jwt.helper';
import { AdminsDataAcceess } from '../../dataAccess/admins.dataAccess';
import { adminObj } from '../../DTO/admin.dto';

@Injectable()
export class ValidAdminMiddleware implements NestMiddleware {
  constructor(
    private readonly adminsDataAcceess: AdminsDataAcceess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.session.admin) {
      res.locals.admin = req.session.admin;
      next();
    } else if (
      req.header('ehghagh_jwt') &&
      req.header('ehghagh_jwt').length > 0
    ) {
      const token = req.header('ehghagh_jwt');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        throw new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'INVALID_TOKEN',
          },
          HttpStatus.FORBIDDEN,
        );
      }
      const admin = await this.adminsDataAcceess.findById(
        tokenValues['adminId'],
      );
      if (!admin) {
        throw new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'ACCESS_DENIED',
          },
          HttpStatus.FORBIDDEN,
        );
      }
      if (admin.jwtToken === token) {
        req.session.admin = admin;
        res.locals.admin = admin;
        next();
      } else {
        throw new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'ACCESS_DENIED',
          },
          HttpStatus.FORBIDDEN,
        );
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'ACCESS_DENIED',
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
@Injectable()
export class CheckAdminMiddleware implements NestMiddleware {
  constructor(
    private readonly adminsDataAcceess: AdminsDataAcceess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.session.admin) {
      res.locals.admin = req.session.admin;
      next();
    } else if (
      req.header('ehghagh_jwt') &&
      req.header('ehghagh_jwt').length > 0
    ) {
      const token = req.header('ehghagh_jwt');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        next();
      }
      const admin = await this.adminsDataAcceess.findById(
        tokenValues['adminId'],
      );
      if (!admin) {
        next();
      }
      if (admin.jwtToken === token) {
        const adminObject = adminObj(admin, token);
        req.session.admin = adminObject;
        res.locals.admin = adminObject;
        next();
      } else {
        next();
      }
    } else {
      next();
    }
  }
}
