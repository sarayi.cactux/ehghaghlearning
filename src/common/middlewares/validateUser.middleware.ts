import {
  Injectable,
  NestMiddleware,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { Jwt } from '../helpers/jwt.helper';
import { UsersDataAcceess } from '../../dataAccess/users.dataAccess';
import { userObj } from '../../DTO/user.dto';

@Injectable()
export class ValidUserMiddleware implements NestMiddleware {
  constructor(
    private readonly usersDataAcceess: UsersDataAcceess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    this.usersDataAcceess.deleteSmsRqs();
    if (req.session.user) {
      res.locals.user = req.session.user;
      next();
    } else if (
      req.header('ehghagh_jwt') &&
      req.header('ehghagh_jwt').length > 0
    ) {
      const token = req.header('ehghagh_jwt');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        throw new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'INVALID_TOKEN',
          },
          HttpStatus.FORBIDDEN,
        );
      }
      const activeToken = await this.usersDataAcceess.findUserToken(token);
      if (!activeToken) {
        throw new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'ACCESS_DENIED',
          },
          HttpStatus.FORBIDDEN,
        );
      } else {
        const user = userObj(activeToken.User, token);
        req.session.user = user;
        res.locals.user = user;
        next();
      }
    } else {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'ACCESS_DENIED',
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
@Injectable()
export class CheckUserMiddleware implements NestMiddleware {
  constructor(
    private readonly usersDataAcceess: UsersDataAcceess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.session.user) {
      res.locals.user = req.session.user;
      next();
    } else if (
      req.header('ehghagh_jwt') &&
      req.header('ehghagh_jwt').length > 0
    ) {
      const token = req.header('ehghagh_jwt');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        next();
      }
      const activeToken = await this.usersDataAcceess.findUserToken(token);
      if (!activeToken) {
        next();
      } else {
        const user = userObj(activeToken.User, token);
        req.session.user = user;
        res.locals.user = user;
        next();
      }
    } else {
      next();
    }
  }
}
@Injectable()
export class ValidateHeaderMiddleware implements NestMiddleware {
  constructor(private readonly jwt: Jwt) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.session.user) {
      res.locals.user = req.session.user;
      next();
    } else if (
      req.header('ehghagh_jwt') &&
      req.header('ehghagh_jwt').length > 0
    ) {
      const token = req.header('ehghagh_jwt');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        throw new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'ACCESS_DENIED',
          },
          HttpStatus.FORBIDDEN,
        );
      }
      next();
    } else {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'ACCESS_DENIED',
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
