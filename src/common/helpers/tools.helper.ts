import smsRequest = require('request');

export class Tools {
  convertToSlug(Text) {
    return Text.toLowerCase()
      .replace(/ /g, '-')
      .replace(/-{6}/g, '-')
      .replace(/-{5}/g, '-')
      .replace(/-{4}/g, '-')
      .replace(/-{3}/g, '-')
      .replace(/--/g, '-')
      .replace(/,/g, '')
      .replace(/،/g, '')
      .replace(/#/g, '')
      .replace(/@/g, '')
      .replace(/$/g, '')
      .replace(/!/g, '')
      .replace(/&/g, '')
      .replace('(', '')
      .replace(')', '')
      .replace('(', '')
      .replace(')', '')
      .replace(/؟/g, '')
      .replace('?', '')
      .replace(/[^\w\s-]/g, '')
      .replace(/[\s_-]+/g, '')
      .replace(/^-+|-+$/g, '');
  }
  normalize(text) {
    text
      .replace(/ /g, '-')
      .replace(/-{6}/g, '-')
      .replace(/-{5}/g, '-')
      .replace(/-{4}/g, '-')
      .replace(/-{3}/g, '-')
      .replace(/--/g, '-')
      .replace(/,/g, '')
      .replace(/،/g, '')
      .replace(/#/g, '')
      .replace(/@/g, '')
      .replace(/$/g, '')
      .replace(/!/g, '')
      .replace(/&/g, '')
      .replace('(', '')
      .replace(')', '')
      .replace('(', '')
      .replace(')', '')
      .replace(/؟/g, '')
      .replace('?', '')
      .replace(/[^\w\s-]/g, '')
      .replace(/[\s_-]+/g, '')
      .replace(/^-+|-+$/g, '');
    return text;
  }
  toInt(str) {
    str = str.replace(/٠/g, '0');
    str = str.replace(/١/g, '1');
    str = str.replace(/٢/g, '2');
    str = str.replace(/٣/g, '3');
    str = str.replace(/٤/g, '4');
    str = str.replace(/٥/g, '5');
    str = str.replace(/٦/g, '6');
    str = str.replace(/٧/g, '7');
    str = str.replace(/٨/g, '8');
    str = str.replace(/٩/g, '9');
    str = str.replace(/۰/g, '0');
    str = str.replace(/۱/g, '1');
    str = str.replace(/۲/g, '2');
    str = str.replace(/۳/g, '3');
    str = str.replace(/۴/g, '4');
    str = str.replace(/۵/g, '5');
    str = str.replace(/۶/g, '6');
    str = str.replace(/۷/g, '7');
    str = str.replace(/۸/g, '8');
    str = str.replace(/۹/g, '9');
    return str;
  }
  checkMime(filename) {
    const i = filename.lastIndexOf('.');
    const memeType = filename.substr(i + 1);
    const imgList = ['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
    const videoList = ['mp4', 'MP4'];
    const musicList = ['mp3', 'wav', 'MP3', 'WAV'];
    const fileList = ['xls', 'xlsx', 'pdf', 'doc', 'docx', 'apk'];
    if (imgList.indexOf(memeType) !== -1) {
      return 1;
    } else if (videoList.indexOf(memeType) !== -1) {
      return 3;
    } else if (fileList.indexOf(memeType) !== -1) {
      return 0;
    } else if (musicList.indexOf(memeType) !== -1) {
      return 2;
    }
    return 20;
  }
  codeCreator() {
    return '1234';
    const val = Math.floor(100000 + Math.random() * 899999);
    return val;
  }
  async sendSmsCode(phone, code) {
    return;
    const receptor = phone;
    const template = 'token';
    const type = 'sms';
    const apiKey =
      '5249504A36753150717256586244316A6A644449686D4E7072414B324638686F5953342F704A39523238413D';
    const url = `https://api.kavenegar.com/v1/${apiKey}/verify/lookup.json`;

    const options = {
      url,
      method: 'POST',
      form: {
        token: code,
        receptor: receptor,
        template: template,
        type: type,
      },
    };

    const result = await smsRequest(options);
    return result;
  }
  async sendSms(phone, text) {
    return;
    const receptor = phone;
    const template = 'token';
    const type = 'sms';
    const apiKey =
      '5249504A36753150717256586244316A6A644449686D4E7072414B324638686F5953342F704A39523238413D';
    const url = `https://api.kavenegar.com/v1/${apiKey}/verify/lookup.json`;

    const options = {
      url,
      method: 'POST',
      form: {
        token: text,
        receptor: receptor,
        template: template,
        type: type,
      },
    };

    const result = await smsRequest(options);
    return result;
  }
}
